

function validaForm( formulario )
{
    let valida = true;
    formulario.find(':required').each( function ()
    {
        if ($(this).val() == "" || $(this).val() == null)
            valida = false;
    });
    return valida;
}