<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function build_portlet($tarea, $idPerfil, $puntaje, $prueba=false)
{
    $prioridad_class = "";
    switch ($tarea->prioridad_id) {
        case 1:$prioridad_class="bg-success";break;
        case 2:$prioridad_class="bg-warning";break;
        case 3:$prioridad_class="bg-danger";break;
    }
    switch ($tarea->dificultad_id) {
        case 1:$dificultad="<span class='badge bg-teal'><i class='fe fe-flag'></i> ".$tarea->dificultad."</span>";break;
        case 2:$dificultad="<span class='badge bg-info'><i class='fe fe-flag'></i> ".$tarea->dificultad."</span>";break;
        case 3:$dificultad="<span class='badge bg-yellow'><i class='fe fe-flag'></i> ".$tarea->dificultad."</span>";break;
        case 4:$dificultad="<span class='badge bg-purple'><i class='fe fe-flag'></i> ".$tarea->dificultad."</span>";break;
        case 5:$dificultad="<span class='badge bg-red'><i class='fe fe-flag'></i> ".$tarea->dificultad."</span>";break;
    }
    
    $html = '<div class="portlet" id="'.$tarea->id.'" id-usuario="'.$tarea->usuario_id.'">
        <div class="portlet-header">
            <span class="status-icon '.$prioridad_class.'"></span>'.$tarea->codigo.'
            <label>'.$tarea->nombre.'</label>
            <div id="cont-label-'.$tarea->id.'">';

    if ( $prueba )
        switch ($prueba->estado_id) {
            case 10: $html .= "<span class='badge bg-yellow'>QA PENDIENTE</span>"; break;
            case 11: $html .= "<span class='badge bg-teal'>QA EN PROCESO</span>"; break;
            case 12: $html .= "<span class='badge bg-green btn-observaciones' tarea='".$tarea->id."'>QA FINALIZADO</span>"; break;
            case 13: $html .= "<span class='badge bg-red btn-observaciones' tarea='".$tarea->id."'>QA RECHAZADO</span>"; break;
        }
    if ($tarea->estado_id == 2 && $tarea->detenida == 't')
        $html .= "<span id='label-flujo-".$tarea->id."' class='badge badge-warning'>DETENIDA</span>";

    $html .= '</div>
            </div>
        <div class="portlet-content" style="display:none">
            <ul>
                <li><b>Fecha inicio:</b></li>
                <li>'.formatDate($tarea->fecha).'</li>';
    $btnCorrerPausar = "";
    if ($tarea->estado_id != 1)
    {
        $html .= '
        <li><b>Tiempo transcurrido:</b></li>
        <li class="time-run" id-time-tarea="'.$tarea->id.'" >'.$tarea->tiempo_transcurrido.' hh:mm</li>';
    }
    $html .= '<li><b>Horas asignadas:</b></li>
            <li>'.$tarea->horas_estimadas.' hrs.</li>
            <li><b>Dificultad:</b></li>
            <li>'.$dificultad.'</li>';
    
    if ($tarea->estado_id > 2)
    {
        $html .= '
        <li><b>Puntaje obtenido</b></li>
        <li>'.$puntaje.' pts.</li>';
    }

    if($idPerfil == 4) 
    {
        $title = "Detener tarea";
        $classBtn = "btn-warning";
        $classIcon = "fe-pause";
        if ($tarea->detenida == 't') {
            $title = "Iniciar tarea";
            $classBtn = "btn-success";
            $classIcon = "fe-play";
        }
            
        $html .= ' <li>
            <div class="cont-btn-control-tarea">
                '.(($tarea->estado_id==2)?'<div>
                    <button title="'.$title.'" detenida="'.$tarea->detenida.'" class="btn '.$classBtn.' btn-block flujo-tarea" id-tarea="'.$tarea->id.'"><i class="fe '.$classIcon.'"></i></button>
                </div>':"").'
                <div>
                    <button title="Detalle de la tarea" class="btn btn-info btn-block detalle-tarea" id-tarea="'.$tarea->id.'"><i class="fe fe-list"></i></button>
                </div>
                <div>
                    <button title="Eliminar tarea" class="btn btn-danger btn-block remove-tarea" id-tarea="'.$tarea->id.'"><i class="fe fe-trash-2"></i></button>
                </div>
            </div>
        </li>';
    }
    else if($idPerfil == 1)
    {
        $title = "Detener tarea";
        $classBtn = "btn-warning";
        $classIcon = "fe-pause";
        if ($tarea->detenida == 't') {
            $title = "Iniciar tarea";
            $classBtn = "btn-success";
            $classIcon = "fe-play";
        }

        $html .= ' <li>
            <div class="cont-btn-control-tarea">';
        if($tarea->estado_id==2)
            $html .= '<div>
                <button title="'.$title.'" detenida="'.$tarea->detenida.'" class="btn '.$classBtn.' btn-block flujo-tarea" id-tarea="'.$tarea->id.'"><i class="fe '.$classIcon.'"></i></button>
            </div>
            <div>
                <button title="Detalle de la tarea" class="btn btn-info btn-block detalle-tarea" id-tarea="'.$tarea->id.'"><i class="fe fe-list"></i></button>
            </div>';
        elseif($tarea->estado_id!=2)
            $html .= '<button title="Detalle de la tarea" class="btn btn-info btn-block detalle-tarea" id-tarea="'.$tarea->id.'"><i class="fe fe-list"></i></button>';
        $html .= '</div>
        </li>';
    }
    $html .= '</ul>
            </div>
        </div>';
    
    return $html;
}

function tiempoTranscurrido($fechaInicio, $fechaFin)
{
    $arrFechaIni = explode(' ', $fechaInicio);
    $arrFecha1 = explode("-", $arrFechaIni[0]);
    $arrHora1 = explode(":", $arrFechaIni[1]);

    $fecha1 = new DateTime($arrFecha1[0]."-".$arrFecha1[1]."-".$arrFecha1[2]." ".$arrHora1[0].":".$arrHora1[1]);
    $fecha2 = new DateTime($fechaFin);
    
    $diff = $fecha1->diff($fecha2);

    $horas = $diff->h + (24 * $diff->d);
    $horas = ($horas > 9) ? $horas : "0".$horas;
    $minutos = ($diff->i > 9) ? $diff->i : "0".$diff->i;

    $data['time'] = "$horas:$minutos";

    return $data;
}

function get_menu ( $index ) 
{
    $menu = array( 
        "home"              => "", 
        "pizarra"           => "", 
        "testing"           => "", 
        "proyectos"         => "", 
        "usuarios"          => "", 
        "backlogs"          => "", 
        "historial_tareas"  => "",
        "historial_tickets" => "",
        "tickets"           => "" 
    );
    $menu[ $index ] = "active";
    return $menu;
}

function exist_tarea_asignada ()
{
    $ci = & get_instance();
    $ci->load->model('tarea_model');

    $exist_new_tarea = "false";
    if ( $ci->tarea_model->exist_tareas_asignada( $ci->session->userdata( "id" ) ) )
        $exist_new_tarea = "true";

    return $exist_new_tarea;
}

function estadoByIdForHistorial( $idEstado )
{
    $estado = "Sin estado";
    switch ($idEstado) {
        case 1: $estado = "Tarea asignada"; break;
        case 2: $estado = "Tarea en desarrollo"; break;
        case 3: $estado = "Tarea entregada"; break;
        case 4: $estado = "Tarea en testing"; break;
        case 5: $estado = "Tarea finalizada"; break;
        case 6: $estado = "Tarea merged"; break;
        case 7: $estado = "Tarea eliminada"; break;
        case 8: $estado = "Tarea en producción"; break;
        case 9: $estado = "Tarea activa"; break;
        case 10: $estado = "Tarea detenida"; break;
        case 11: $estado = "Tarea tomada"; break;
    }
    return $estado;
}

function estadoByIdForHistorialTicket( $idEstado )
{
    $estado = "Sin estado";
    switch ($idEstado) {
        case 1: $estado = "Ticket asignado"; break;
        case 2: $estado = "Ticket en desarrollo"; break;
        case 3: $estado = "Ticket entregado"; break;
        case 14: $estado = "Ticket completado"; break;
        case 15: $estado = "Ticket rechazado"; break;
        case 16: $estado = "Ticket reasignado"; break;
        case 7: $estado = "Ticket eliminado"; break;
    }
    return $estado;
}

function colorEstadosHitorial($idEstado)
{
    $color = "";
    switch ($idEstado) {
        case 1: $color = "bg-blue"; break;
        case 2: $color = "bg-orange"; break;
        case 3: $color = "bg-green"; break;
        case 4: $color = "bg-yellow"; break;
        case 5: $color = "bg-teal"; break;
        case 6: $color = "bg-purple"; break;
        case 7: $color = "bg-red"; break;
        case 8: $color = "bg-info"; break;
    }
    return $color;
}

function colorEstadosHitorialTickets($idEstado)
{
    $color = "";
    switch ($idEstado) {
        case 1: $color = "bg-info"; break;
        case 2: $color = "border"; break;
        case 3: $color = "bg-yellow"; break;
        case 14: $color = "bg-green"; break;
        case 15: $color = "bg-red"; break;
        case 16: $color = "bg-red"; break;
        case 7: $color = "bg-grey"; break;
    }
    return $color;
}

function calcularRanking($horasEstimadas, $tiempoTranscurrido, $puntajeDificultad)
{
    $horasEstimadas = explode(":", $horasEstimadas);
    $he = (int)$horasEstimadas[0];
    $he += (count($horasEstimadas) > 1) ? (((int)$horasEstimadas[1] * 1) / 60) : 0;
    
    $tiempoTranscurrido = explode(":", $tiempoTranscurrido);
    $tt = (int)$tiempoTranscurrido[0];
    $tt += (count($tiempoTranscurrido) > 1) ? (((int)$tiempoTranscurrido[1] * 1) / 60) : 0;

    $porcentajeHoras = ($tt * 100) / $he;
    $porcentajeExtra = 100 - $porcentajeHoras;
    $puntajeExtra = ($puntajeDificultad * $porcentajeExtra) / 100;
    $puntajeObtenido = $puntajeDificultad + $puntajeExtra;

    $porcentajeCumplimiento = ($puntajeObtenido * 100) / $puntajeDificultad;
    if($porcentajeCumplimiento > 100)
        $porcentajeCumplimiento = 100;
    elseif($porcentajeCumplimiento < 0)
        $porcentajeCumplimiento = 0;

    $data['puntajeObtenido'] = round($puntajeObtenido, 0);
    $data['procentajeCumplimiento'] = round($porcentajeCumplimiento, 1);

    return $data;
}

function formatDate( $fecha )
{
    $newFecha = "Sin fecha...";
    if ($fecha != "")
    {
        $arrFH = explode(" ", $fecha);
        $arrF = explode("-", $arrFH[0]);
        $arrH = explode(":", $arrFH[1]);
        $newFecha = $arrF[2] . "-" . $arrF[1] . "-" . $arrF[0] . " " . $arrH[0] . ":" . $arrH[1];
    }

    return $newFecha;
}

?>