<?php
class Ticket_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getBydepartamento($departamento)
    {
        $where = "t.estado_id != 7 and (t.departamento_creador_id = '$departamento' OR t.departamento_destino_id = '$departamento')";

        $result = $this->db->select('t.id, t.detalle, t.asunto, t.estado_id, e.estado, t.prioridad_id, p.prioridad,
        t.departamento_creador_id, d.nombre AS departamento, t.departamento_destino_id, t.fecha')
                            ->join('estado e', 'e.id = t.estado_id')
                            ->join('prioridad p', 'p.id = t.prioridad_id')
                            ->join('departamento d', 'd.id = t.departamento_creador_id')
                            ->where( $where )
                            ->limit(20)
                            ->order_by('e.orden DESC, t.id DESC')
                            ->get('ticket t');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function getByDepartamentoHistorial($departamento, $start, $length, $search)
    {
        $totalRegistros = $this->db->count_all_results('ticket');
        $this->db->flush_cache();
        
        $where = "t.departamento_creador_id = '$departamento' OR t.departamento_destino_id = '$departamento'";

        $result = $this->db->select('t.id, t.detalle, t.asunto, t.estado_id, e.estado, t.prioridad_id, p.prioridad,
        t.departamento_creador_id, d.nombre AS departamento, t.departamento_destino_id, t.fecha')
                            ->join('estado e', 'e.id = t.estado_id')
                            ->join('prioridad p', 'p.id = t.prioridad_id')
                            ->join('departamento d', 'd.id = t.departamento_creador_id')
                            ->where( $where );
        if ( $search != null || $search != "" )
        {   
            $this->db->like('t.asunto', $search, 'both');
        }
        $this->db->limit($length, $start);
        $result = $this->db->get('ticket t');
        
        return ($result->num_rows() > 0) ? 
            array(
                'total_rows'=>$totalRegistros, 
                'rows'=>$result->num_rows(), 
                'result'=>$result->result()
            ): 
            array(
                'total_rows'=>0, 
                'rows'=>0, 
                'result'=>false
            );
    }

    public function save( $data )
    {
        return $this->db->insert('ticket', $data);
    }

    public function getPrioridad()
    {
        $result = $this->db->get('prioridad');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function getTicket( $id )
    {
        $result = $this->db->get_where('ticket', array('id'=>$id));
        return ( $result->num_rows() > 0 ) ? $result->row() : false;
    }

    public function update( $id, $data )
    {
        return $this->db->where('id', $id)
                            ->update('ticket', $data);
    }

    public function countTicket( $dataWehere )
    {
        return $this->db->where($dataWehere)
                        ->count_all_results('ticket');
    }

    public function getForNotificacion( $dataWhere )
    {
        $result = $this->db->select('id')
                            ->where( $dataWhere )
                            ->get('ticket');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }
}
?>