<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getHistorial ( $id , $tipo)
    {
        $this->db->select('h.fecha, h.historial, h.estado_historico, u.nombre');
        $this->db->join('tarea t', 't.id = h.tarea_id');
        $this->db->join('usuario u', 'u.id = t.usuario_id');
        $this->db->where(array('h.tarea_id'=>$id, 'h.tipo'=>$tipo));
        $result = $this->db->get('historial h');

        return ( $result->num_rows() > 0) ? $result->result() : FALSE;
    }

    public function getHistorialTicket ( $id , $tipo)
    {
        $this->db->select('h.fecha, h.historial, h.estado_historico, d.nombre, t.departamento_destino_id');
        $this->db->join('ticket t', 't.id = h.ticket_id');
        $this->db->join('departamento d', 'd.id = t.departamento_creador_id');
        $this->db->where(array('h.ticket_id'=>$id, 'h.tipo'=>$tipo));
        $result = $this->db->get('historial h');

        return ( $result->num_rows() > 0) ? $result->result() : FALSE;
    }

    public function save($data)
    {
        return $this->db->insert('historial', $data);
    }

}