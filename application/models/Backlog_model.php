<?php
class Backlog_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAll( $conf )
    {	
        $totalRegistros = $this->db->where('backlog.estado_id', 9)->count_all_results('backlog');
        $this->db->flush_cache();

        $this->db->select('usuario.nombre, usuario.imagen, backlog.asunto, backlog.id, proyecto.descripcion, backlog.backlog, backlog.fecha, backlog.codigo');
        $this->db->join('proyecto', 'backlog.proyecto_id = proyecto.id');
        $this->db->join('usuario', 'usuario.id = backlog.usuario_id');
        $this->db->limit($conf['length'], $conf['start']);

        if($conf['length'])
            if ( $conf['search'] != null || $conf['search'] != "" )
                $this->db->like('usuario.nombre', $conf['search'], 'both');

        $this->db->where('backlog.estado_id', 9);
        $result = $this->db->get('backlog');
          
      	return ($result->num_rows() > 0) ? 
            array(
                'total_rows'=>$totalRegistros, 
                'rows'=>$result->num_rows(), 
                'result'=>$result->result()
                ) : 
            array(
                'total_rows'=>0, 
                'rows'=>0, 
                'result'=>false
            );
    }

    public function get($backlog)
    {
        $result = $this->db->select('b.id, b.backlog, b.usuario_id, b.fecha, b.codigo,
        b.proyecto_id, b.sn, b.estado_id, b.asunto, p.descripcion AS proyecto')
                                    ->join('proyecto p', 'p.id = b.proyecto_id')
                                    ->where('b.id', $backlog)
                                    ->get('backlog b');
        return ($result->num_rows() == 1) ? $result->row() : false;
    }

    public function save(&$data)
    {
        $this->db->insert('backlog',$data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $this->db->where('id',$id);
        if($this->db->update('backlog',$data))
            return true;
        else
            return false;
    }

    public function getCodigo($idProyecto)
    {
        $this->db->select_max('sn');
        $this->db->where('proyecto_id', $idProyecto);
        $query = $this->db->get('backlog');

        $data['sn'] = ($query->num_rows() > 0) ? $query->row()->sn : 0;
        
        $this->db->flush_cache();

        $this->db->select('codigo');
        $this->db->where('id', $idProyecto);
        $query = $this->db->get('proyecto');

        $data['codigo'] = ($query->num_rows() > 0) ? $query->row()->codigo : "XXX";

        return $data;
    }

    public function removeBacklog( $backlog )
    {
        $this->db->where('id', $backlog);
        $this->db->set('estado_id', 7);
        $result = $this->db->update('backlog');

        return $this->db->affected_rows();
    }

    public function getByTarea( $tarea )
    {
        $result = $this->db->select('b.id')
                            ->join('tarea t', 't.backlog_id = b.id')
                            ->where('t.id', $tarea)
                            ->get('backlog b');
                            
        return ($result->num_rows() == 1) ? $result->row() : false;
    }

    public function saveAdjunto($dataInsert)
    {
        return $this->db->insert_batch('adjunto', $dataInsert);
    }

    public function getAdjuntosByBacklog($backlogId)
    {
        $result = $this->db->get_where('adjunto', array('backlog_id' => $backlogId));
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function eliminarAdjuntoById($id) 
    {
        return $this->db->delete('adjunto', array('id'=>$id));
    }
}
?>