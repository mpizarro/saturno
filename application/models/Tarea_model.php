<?php
class Tarea_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAll( $where, $conf )
    {	
        $numRegistros = $this->db->where( $where )->count_all_results('tarea');
        $this->db->flush_cache();

        $this->db->select('tarea.id, usuario.nombre, usuario.imagen, tarea.id, proyecto.descripcion, tarea.detenida, 
                        backlog.backlog, tarea.fecha, tarea.fecha_inicio, tarea.fecha_fin, 
                        tarea.tiempo_transcurrido, backlog.codigo, dificultad.dificultad, tarea.dificultad_id, 
                        dificultad.puntaje, estado.estado, tarea.estado_id, prioridad.prioridad, tarea.usuario_id, 
                        tarea.prioridad_id, tarea.horas_estimadas');
        if($conf['length'])
        {
            if ( $conf['search'] != null || $conf['search'] != "" )
                $this->db->like('usuario.nombre', $conf['search'], 'both');
            
            $this->db->limit($conf['length'], $conf['start']);
        }
        $this->db->join('backlog', 'tarea.backlog_id = backlog.id')
        ->join('proyecto', 'backlog.proyecto_id = proyecto.id')
        ->join('usuario', 'usuario.id = tarea.usuario_id')
        ->join('dificultad', 'dificultad.id = tarea.dificultad_id')
        ->join('prioridad', 'prioridad.id = tarea.prioridad_id')
        ->join('estado', 'estado.id = tarea.estado_id')
        ->order_by('tarea.id DESC')
        ->where( $where );

        if ($conf['limit'])
            $this->db->limit($conf['limit']);
            
        $query = $this->db->get('tarea');
          
        return ($query->num_rows() > 0) ?
            array(
                'total_rows'=>$numRegistros, 
                'rows'=>$query->num_rows(), 
                'result'=>$query->result()
                ) : 
            array(
                'total_rows'=>0, 
                'rows'=>0, 
                'result'=>false
            );
    }

    public function getTarea($id_tarea)
    {	
        $this->db->select('usuario.nombre, usuario.imagen, tarea.id, proyecto.descripcion, tarea.detenida, 
                        backlog.backlog, tarea.fecha, tarea.fecha_inicio, tarea.fecha_fin, 
                        tarea.tiempo_transcurrido, backlog.codigo, dificultad.dificultad, tarea.dificultad_id, 
                        dificultad.puntaje, estado.estado, tarea.estado_id, prioridad.prioridad, 
                        tarea.prioridad_id, tarea.horas_estimadas');
        $this->db->from('tarea');
        $this->db->join('backlog', 'tarea.backlog_id = backlog.id');
        $this->db->join('proyecto', 'backlog.proyecto_id = proyecto.id');
        $this->db->join('usuario', 'usuario.id = tarea.usuario_id');
        $this->db->join('dificultad', 'dificultad.id = tarea.dificultad_id');
        $this->db->join('prioridad', 'prioridad.id = tarea.prioridad_id');
        $this->db->join('estado', 'estado.id = tarea.estado_id');
        $this->db->where('tarea.id', $id_tarea);
      	$query = $this->db->get();
      	return ($query->num_rows() > 0) ? $query->row() : false;
    }

    public function estaDetenida($tarea)
    {
        $result = $this->db->select('detenida')
                            ->where('id', $tarea)
                            ->get('tarea t');

        return ($result->num_rows() > 0) ? $result->row() : fasle;
    }

    public function save(&$data)
    {
        $return['resp'] = $this->db->insert('tarea', $data);
        $return['tarea_id'] = $this->db->insert_id();
        return $return;
    }

    public function countTareas ( $dataWehere )
    {
        return $this->db->where($dataWehere)
                        ->count_all_results('tarea');
    }

    public function getDificultad ()
    {
        $result = $this->db->get('dificultad');

        return ($result->num_rows() > 0) ? $result->result() : false ;
    }

    public function remove ($tarea)
    {
        $this->db->set('estado_id', 7);
        $this->db->where('id', $tarea);
        return $this->db->update('tarea');
    }

    public function update_estado ( $tarea, $estado, $fecha )
    {
        $this->db->set('estado_id', $estado);
        if ( $fecha != false )
        {
            $this->db->set('fecha', $fecha);
            $this->db->set('fecha_inicio', $fecha);
        }
        $this->db->where('id', $tarea);
        return $this->db->update('tarea');
    }

    public function getDetalle( $id_tarea )
    {
        $this->db->select('backlog.backlog');
        $this->db->join('backlog', 'tarea.backlog_id = backlog.id');
        $this->db->where('tarea.id', $id_tarea);
        $query = $this->db->get('tarea');
          
      	return ($query->num_rows() > 0) ? $query->row() : false;
    }

    public function correrPausar( $tarea, $data )
    {
        return $this->db->where('id', $tarea)
                        ->update('tarea', $data);
    }

    public function getForNotificacion( $dataWhere )
    {
        $result = $this->db->select('id')
                            ->where( $dataWhere )
                            ->get('tarea');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function saveRanking($data)
    {
        return $this->db->insert('ranking', $data);
    }

    public function updateRanking($dataRanking, $id)
    {
        return $this->db->where('id', $id)
                        ->update('ranking', $dataRanking);
    }

    public function getRanking($tareaId)
    {
        $result = $this->db->get_where('ranking', array('tarea_id'=>$tareaId));
        return ( $result->num_rows() == 1 ) ? $result->row() : false;
    }

    public function getRankingByUsuario($usuarioId)
    {
        $result = $this->db->get_where('ranking', array('usuario_id'=>$usuarioId));
        return ($result->num_rows() > 0) ? $result->result() : false;
    }
}
?>