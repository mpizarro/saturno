<?php
class Usuario_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAll($conf)
    {	
        $totalRegistros = $this->db->count_all_results('usuario');
        $this->db->flush_cache();

        $this->db->select('usuario.id, usuario.nombre, usuario.usuario, usuario.imagen, perfil.perfil');
        $this->db->join('perfil', 'perfil.id = usuario.perfil_id');
        $this->db->where('usuario.estado_id', 9);
        if($conf['length'])
        {
            if ( $conf['search'] != null || $conf['search'] != "" )
                $this->db->like('usuario.nombre', $conf['search'], 'both');

            $this->db->limit($conf['length'], $conf['start']);
        }
        $query = $this->db->get('usuario');

      	return ($query->num_rows() > 0) ?
            array(
                'total_rows'=>$totalRegistros, 
                'rows'=>$query->num_rows(), 
                'result'=>$query->result()
                ) :
            array(
                'total_rows'=>0, 
                'rows'=>0, 
                'result'=>false
            );
    }

    public function getUserForTesting( $user )
    {	
        $this->db->select('usuario.id, usuario.nombre, usuario.imagen, usuario.usuario, perfil.perfil');
        $this->db->join('perfil', 'perfil.id = usuario.perfil_id');
        $this->db->where(array('usuario.estado_id'=>9, 'usuario.id !='=>$user, 'usuario.is_testing'=>true));
        $this->db->order_by('usuario.id');
        $query = $this->db->get('usuario');

      	return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function getUsuario( $user )
    {
        $this->db->select('usuario.id, usuario.nombre, usuario.imagen, usuario.usuario, usuario.imagen, 
        usuario.perfil_id, perfil.perfil, is_testing, usuario.departamento_id, departamento.nombre AS departamento');
        $this->db->join('perfil', 'perfil.id = usuario.perfil_id');
        $this->db->join('departamento', 'departamento.id = usuario.departamento_id');
        $this->db->where('usuario.estado_id', 9);
        $this->db->where('usuario.id', $user);
        $query = $this->db->get('usuario');

      	return ($query->num_rows() > 0) ? $query->row() : false;
    }

    public function deleteUser( $user )
    {
        $this->db->where('id', $user);
        $this->db->update('usuario', array('estado_id'=>7));
        return $this->db->affected_rows();
    }

    public function updateUser( $user, $data )
    {
        $this->db->where('id', $user);
        $this->db->update('usuario', $data);
        return $this->db->affected_rows();
    }

    public function saveUser( $data )
    {
        return $this->db->insert('usuario', $data);
    } 
    
    public function saveAvatar($usuario, $img)
    {
        return $this->db->where('id', $usuario)
                        ->update('usuario', array('imagen'=>$img));
    }

    public function getImg( $id )
    {
        $result = $this->db->select('imagen')
                            ->where('id', $id)
                            ->get('usuario');
        return $result->row();
    }

    public function checkPass( $user, $pass )
    {
        $result = $this->db->select('password')
                            ->where('id', $user)
                            ->get('usuario');
        return ($result->num_rows() == 1) ? password_verify($pass, $result->row()->password) : false;
    }

    public function getPerfiles()
    {
        $result = $this->db->get('perfil');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function getDepartamento()
    {
        $result = $this->db->get('departamento');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function getCategorias()
    {
        $result = $this->db->order_by('id', 'DESC')
                            ->get('categoria');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }
}
?>