<?php

class PruebaCruzada_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function asignarPruebaCruzada($data)
    {
        $this->db->insert('prueba_cruzada', $data);
        return $this->db->affected_rows();
    }

    public function countPruebaCruzada($data_where)
    {
        return $this->db->where($data_where)
                            ->count_all_results('prueba_cruzada');
    }

    public function existThisPruebaCruzada($solicitante, $solicitado, $tarea)
    {
        $result = $this->db->get_where('prueba_cruzada', array('usuario_solicitado'=>$solicitado, 'usuario_silicitante'=>$solicitante, 'tarea_id'=>$tarea));
        return ($result->num_rows() > 0) ? TRUE : FASLE;
    } 

    public function getAll() 
    {
        $this->db->select('pc.id, pc.estado_id, pc.usuario_solicitado, u.imagen, u.nombre AS solicitante, b.codigo, b.backlog AS descripcion, t.id AS id_tarea, t.prioridad_id');
        $this->db->join('tarea t', 't.id = pc.tarea_id');
        $this->db->join('usuario u', 'u.id = pc.usuario_solicitante');
        $this->db->join('backlog b', 'b.id = t.backlog_id');
        $this->db->where('t.estado_id', 4);
        $this->db->where('pc.estado_id !=', 12);
        $this->db->where('pc.estado_id !=', 13);
        $result = $this->db->get('prueba_cruzada pc');

        return ($result->num_rows() > 0) ? $result->result() : FALSE;
    }

    public function getByUser($idUser)
    {
        $this->db->select('pc.id, pc.estado_id, pc.usuario_solicitado, u.imagen, u.nombre AS solicitante, b.codigo, b.backlog AS descripcion, t.id AS id_tarea, t.prioridad_id');
        $this->db->join('tarea t', 't.id = pc.tarea_id');
        $this->db->join('usuario u', 'u.id = pc.usuario_solicitante');
        $this->db->join('backlog b', 'b.id = t.backlog_id');
        $this->db->where('pc.usuario_solicitado', $idUser); 
        $this->db->where('t.estado_id', 4);
        $this->db->where('pc.estado_id !=', 12);
        $this->db->where('pc.estado_id !=', 13);
        $result = $this->db->get('prueba_cruzada pc');

        return ($result->num_rows() > 0) ? $result->result() : FALSE;
    }

    public function getByTarea($tarea)
    {
        $result = $this->db->get_where('prueba_cruzada', array('tarea_id'=>$tarea));
        return ($result->num_rows() > 0) ? $result->row() : FALSE;
    }

    public function getIdTareaByPruebaCruzada( $idPruebaCruzada ) 
    {
        $this->db->select('pc.tarea_id')->where('id', $idPruebaCruzada);
        $result = $this->db->get('prueba_cruzada pc');

        return ( $result->num_rows() > 0 ) ? $result->row() : FALSE;
    }

    public function changeEstado($idPruebaCruzada, $estado, $observacion)
    {
        $observacion = ( $observacion ) ? $observacion : null;
        $this->db->where('id', $idPruebaCruzada);
        $this->db->update('prueba_cruzada', array('estado_id'=>$estado, 'observacion'=>$observacion));
        return $this->db->affected_rows();
    }

    public function updatePruebaCruzada($data, $data_where)
    {
        $this->db->where($data_where)
                ->update('prueba_cruzada', $data);
        return $this->db->affected_rows();
    }

    public function delete( $where)
    {
        return $this->db->where( $where )->update('prueba_cruzada', array('estado_id'=>7));
    }

    public function getForNotificacion( $dataWhere )
    {
        $result = $this->db->select('id')
                            ->where( $dataWhere )
                            ->get('prueba_cruzada');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }
}

?>