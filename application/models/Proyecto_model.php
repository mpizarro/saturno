<?php
class Proyecto_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAll($conf)
    {
        $totalRegistros = $this->db->where('proyecto.estado_id', 9)->count_all('proyecto');
        $this->db->flush_cache();

        if($conf['length'])
            $this->db->limit($conf['length'], $conf['start']);
        $this->db->where('proyecto.estado_id', 9);
        $result = $this->db->get('proyecto');

      	return ($result->num_rows() > 0) ? 
            array(
                'total_rows'=>$totalRegistros, 
                'rows'=>$result->num_rows(), 
                'result'=>$result->result()
                ) : 
            array(
                'total_rows'=>0, 
                'rows'=>0, 
                'result'=>false
            );
    }

    public function save_proyecto ( $data )
    {
        return $this->db->insert('proyecto', $data);
    }

    public function remove_proyecto( $id_proyecto )
    {
        $this->db->where('id', $id_proyecto);
        $this->db->set('estado_id', 7);
        return $this->db->update('proyecto');
    }

    public function update_proyecto($id, $data)
    {
        return $this->db->where('id', $id)
                        ->update('proyecto', $data);
    }

    public function getProyectoById($id)
    {
        $result = $this->db->get_where('proyecto', array('id'=>$id));
        return ($result->num_rows() == 1) ? $result->row() : FALSE;
    }
}
?>