<?php

class Login_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function check_login ($username, $password)
    {
        $this->db->select("u.id, u.usuario, u.nombre, u.password, u.perfil_id, p.perfil, 
        d.nombre AS departamento, u.departamento_id");
        $this->db->join("perfil p", "p.id = u.perfil_id");
        $this->db->join("departamento d", "d.id = u.departamento_id");
        $this->db->where("u.usuario", $username);
        $this->db->where("u.estado_id", 9);
        $result = $this->db->get("usuario u");

        $valid = FALSE;

        if ($result->num_rows() > 0)
            if ( password_verify($password, $result->row()->password) )
            {
                $valid = TRUE;
                $user = array(
                    'id'                => $result->row()->id,
                    'username'          => $result->row()->usuario,
                    'name'              => $result->row()->nombre,
                    'perfil_id'         => $result->row()->perfil_id,
                    'perfil'            => $result->row()->perfil,
                    'departamento_id'   => $result->row()->departamento_id,
                    'departamento'      => $result->row()->departamento,
                    'is_logged'         => TRUE
                );
                $this->session->set_userdata($user);
            }
        
        return $valid;
    }

}