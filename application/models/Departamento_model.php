<?php
class Departamento_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAll( $id=false )
    {
        if ( $id )
            $this->db->where('id !=', $id);
        $result = $this->db->get('departamento');
        return ($result->num_rows() > 0) ? $result->result() : false;
    }

    public function getById( $id )
    {
        $result = $this->db->get_where('departamento', array('id'=>$id));
        return ($result->num_rows() > 0) ? $result->row() : false;
    }
}
?>