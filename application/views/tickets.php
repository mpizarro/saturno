

<div class="page-content">
    <div class="container-fluid">

        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div class="row m-0 border" style="width:100%;">
                        <div class="col-6 col-sm-1 col-md-1 col-lg-1 p-0 text-left">
                            <h3 class="card-title">Tickets</h3>
                        </div>
                        <div class="hidden-xs col-sm-10 col-md-10 col-lg-10 p-0 text-center">
                            <div class="row text-center">
                                <div class="col-2">
                                    <p class="h5"><i class="fe fe-circle"></i> Asignada</p>
                                </div>
                                <div class="col-2">
                                    <p class="h5"><i class="fa fa-circle"></i> En desarrollo</p>
                                </div>
                                <div class="col-2">
                                    <p class="h5"><i class="fa fa-circle text-yellow"></i> Entregado</p>
                                </div>
                                <div class="col-2">
                                    <p class="h5"><i class="fa fa-circle text-success"></i> Completado</p>
                                </div>
                                <div class="col-3">
                                    <p class="h5"><i class="fa fa-circle text-danger"></i> Rechazado - Reasignada</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-1 col-md-1 col-lg-1 p-0 text-right">
                            <button id="btn-modal-nuevo" type="button" class="btn btn-info"><i class='fe fe-plus-square'></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-body row table-responsive pt-0">
                    <table id="tabla-usuarios" class="table card-table table-vcenter text-nowrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <thead>
                            <tr>
                                <th class="w-1">No. Id</th>
                                <th>Asunto</th>
                                <th>Prioridad</th>
                                <th>Creado por</th>
                                <th>Para</th>
                                <th>Creado</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="rowsTickets">
                            <tr>
                                <td colspan="8">
                                    <center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal nuevo -->
<div class="modal" tabindex="-1" role="dialog" id="modal-nuevo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-nuevo">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Asunto</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="asunto" id="asunto" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Prioridad</label>
                            <div class="input-icon mb-3">
                                <select  name="prioridad" id="prioridad" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Destinatario</label>
                            <div class="input-icon mb-3">
                                <select name="destino" id="destino" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Requerimiento</label>
                            <div class="input-icon mb-3">
                                <textarea name="requerimiento" id="requerimiento" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-nuevo" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal nuevo -->

<!-- modal update -->
<div class="modal" tabindex="-1" role="dialog" id="modal-update">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-update">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Asunto</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="asunto" id="asunto-update" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Prioridad</label>
                            <div class="input-icon mb-3">
                                <select  name="prioridad" id="prioridad-update" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Destinatario</label>
                            <div class="input-icon mb-3">
                                <select name="destino" id="destino-update" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Requerimiento</label>
                            <div class="input-icon mb-3">
                                <textarea name="requerimiento" id="requerimiento-update" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-update" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal update -->

<!-- modal ver -->
<div class="modal" tabindex="-1" role="dialog" id="modal-ver">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title">Ver ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <p id="ver-detalle"></p>
            </div>

            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-label="Close">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal ver -->

<!-- modal ver rechazo -->
<div class="modal" tabindex="-1" role="dialog" id="modal-ver-rechazo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red text-white">
                <h5 class="modal-title">Ver motivo del rechazo del ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <p id="ver-motivo-rechazo"></p>
            </div>

            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" aria-label="Close">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal ver rechazo -->

<!-- modal rechazar -->
<div class="modal" tabindex="-1" role="dialog" id="modal-rechazar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red text-white">
                <h5 class="modal-title">Rechazar ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-rechazar">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Motivo</label>
                            <div class="input-icon mb-3">
                                <textarea name="motivo" id="motivo" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-rechazar" class="btn btn-info">Rechazar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal rechazar -->

<!-- modal reasignar -->
<div class="modal" tabindex="-1" role="dialog" id="modal-reasignar">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
            <div class="modal-header bg-red text-white">
                <h5 class="modal-title">Reasignar ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-reasignar">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Requerimiento</label>
                            <div class="input-icon mb-3">
                                <textarea name="requerimiento" id="requerimiento-reasignar" rows="5" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-reasignar" class="btn btn-info">Reasignar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal update -->


<script type="text/javascript">

    var id;
    var estado;

    $( document ).ready( function()
    {
        getPrioridad();
        getDepartamentos();
        getTickets();

        setInterval( 'getTickets()', 5000);
    });

    $(document).on("click", "#btn-modal-nuevo", function ()
    {
        $("#asunto").val(null);
        $("#prioridad").val(null);
        $("#destino").val(null);
        $("#requerimiento").val(null);

        $("#modal-nuevo").modal();
    });

    $(document).on("click", "#btn-nuevo", function()
    {
        if ( $("#asunto").val() != "" && $("#prioridad").val() != null && $("#destino").val() != null && $("#requerimiento").val() != "" )
        {
            let data = {
                'asunto' : $("#asunto").val(),
                'prioridad' : $("#prioridad").val(),
                'destino' : $("#destino").val(), 
                'requerimiento' : $("#requerimiento").val()
            };
            $.post( "<?php echo site_url() ?>/ticket/nuevo", data )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.response )
                    toast({type:'success', title:'¡Ticket creado!'});
                else
                    toast({type:'error', title:'Error al crear ticket'});

                getTickets();
                $("#modal-nuevo").modal('hide');
            });
        }
    });

    $(document).on("click", ".btn-modal-update", function ()
    {
        id = $(this).attr('id-ticket');
        $.post( "<?php echo site_url() ?>/ticket/getTicket", {id:id} )
        .done( function ( json )
        {
            json = JSON.parse( json );

            $("#asunto-update").val( json.asunto );
            $("#prioridad-update").val( json.prioridad ).change();
            $("#destino-update").val( json.destino ).change();
            $("#requerimiento-update").val( json.requerimiento );

            $("#modal-update").modal();
        });
    });

    $(document).on("click", "#btn-update", function ()
    {
        if ( $("#asunto-update").val() != "" && $("#prioridad-update").val() != null && $("#destino-update").val() != null && $("#requerimiento-update").val() != "" )
        {
            let data = {
                'id' : id,
                'asunto' : $("#asunto-update").val(),
                'prioridad' : $("#prioridad-update").val(),
                'destino' : $("#destino-update").val(), 
                'requerimiento' : $("#requerimiento-update").val()
            };
            $.post( "<?php echo site_url() ?>/ticket/update", data )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.response )
                    toast({type:'success', title:'¡Ticket editado!'});
                else
                    toast({type:'error', title:'Error al editar ticket'});

                getTickets();
                $("#modal-update").modal('hide');
            });
        }
    });

    $(document).on("click", ".btn-change-estado", function()
    {
        id = $(this).attr('id-ticket');
        estado = $(this).attr('estado');
        let label = "";

        switch (estado) {
            case '2': label = "recibir"; break;
            case '3': label = "entregar"; break;
            case '7': label = "eliminar"; break;
            case '14': label = "completar"; break;
            case '15': label = "rechazar"; break;
        }

        swal({
            title: label.toUpperCase(),
            text: "¿Desea " + label + " este ticket?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'Canceler'
        }).then((result) => 
        {
            if (result.value) 
            {
                $.post( "<?php echo site_url() ?>/ticket/changeEstado", {id:id, estado:estado, motivo:null} )
                .done( function ( json )
                {
                    json = JSON.parse( json );
                    if ( json.response )
                        toast({type:'success', title:'¡Perfecto!'});
                    else
                        toast({type:'error', title:'¡Error!'});
                    getTickets();
                    setStorage();
                });
            }
        })
    });

    $(document).on("click", ".btn-ver", function ()
    {
        id = $(this).attr('id-ticket');
        $.post( "<?php echo site_url() ?>/ticket/getTicket", {id:id} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#ver-detalle").html( json.requerimiento );
            $("#modal-ver").modal();
        });
    });

    $(document).on("click", ".btn-ver-rechazo", function ()
    {
        id = $(this).attr('id-ticket');
        $.post( "<?php echo site_url() ?>/ticket/getTicket", {id:id} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#ver-motivo-rechazo").html( json.motivoRechazo );
            $("#modal-ver-rechazo").modal();
        });
    });

    $(document).on("click", ".btn-modal-rechazar", function ()
    {
        id = $(this).attr('id-ticket');
        estado = $(this).attr('estado');
        $("#motivo").val(null);
        $("#modal-rechazar").modal();
    });

    $(document).on("click", "#btn-rechazar", function ()
    {
        if ( $("#motivo").val() != "" )
        {
            let motivo = $("#motivo").val();
            $.post( "<?php echo site_url() ?>/ticket/changeEstado", {id:id, estado:estado, motivo:motivo} )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.response )
                    toast({type:'success', title:'¡Perfecto!'});
                else
                    toast({type:'error', title:'¡Error!'});
                $("#modal-rechazar").modal('hide');
                getTickets();
            });
        }
    });

    $(document).on("click", ".btn-modal-reasignar", function ()
    {
        id = $(this).attr('id-ticket');
        estado = $(this).attr('estado');
        $.post( "<?php echo site_url() ?>/ticket/getTicket", {id:id} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#requerimiento-reasignar").val( json.requerimiento );
            $("#modal-reasignar").modal();
        });
    });

    $(document).on("click", "#btn-reasignar", function ()
    {
        let requerimiento = $("#requerimiento-reasignar").val();
        if ($("#requerimiento-reasignar").val() != "")
            $.post( "<?php echo site_url() ?>/ticket/changeEstado", {id:id, estado:estado, requerimiento:requerimiento} )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.response )
                        toast({type:'success', title:'¡Perfecto!'});
                else
                    toast({type:'error', title:'¡Error!'});

                $("#modal-reasignar").modal('hide');
                getTickets();
            });
    });

    var getTickets = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/ticket/getTickets",
            success: function ( json )
            {
                json = JSON.parse( json );
                $("#rowsTickets").html( json.rows );
            }
        });
    }

    var getPrioridad = function () 
    {
        $.ajax({
            url: "<?php echo site_url() ?>/ticket/selectPrioridad",
            success: function ( json )
            {
                json = JSON.parse( json );
                $("#prioridad").html( json.select );
                $("#prioridad-update").html( json.select );
            }
        });
    }

    var getDepartamentos = function () 
    {
        $.ajax({
            url: "<?php echo site_url() ?>/ticket/selectDepartamento",
            success: function ( json )
            {
                json = JSON.parse( json );
                $("#destino").html( json.select );
                $("#destino-update").html( json.select );
            }
        });
    }
</script>