<?php header('Access-Control-Allow-Origin: *'); ?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="<?php echo base_url() ?>assets/images/saturno.png" type="image/png"/>
    <!-- Generated: 2018-03-27 13:25:03 +0200 -->
    <title>Saturno</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <link rel="stylesheet" href="<?php echo base_url()."assets/css/my-style.css"; ?>">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Dashboard Core -->
    <link href="<?php echo base_url()."assets/css/dashboard.css"; ?>" rel="stylesheet" />
    <!-- c3.js Charts Plugin -->
    <link href="<?php echo base_url()."assets/plugins/charts-c3/plugin.css"; ?>" rel="stylesheet" />
    <!-- Portlets CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.css">
    <link href="<?php echo base_url() ?>assets/css/portlets.css" rel="stylesheet">
    <!-- TimeLine CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/timeLine.css">

    <!-- JQuery -->
    <script src="<?php echo base_url() ?>assets/js/vendors/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Validador de formularios -->
    <script src="<?php echo base_url() ?>assets/js/validador-furmulario.js"></script>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/datatables.css"/>
</head>

<body class="">
    <div class="page">
        <div class="page-main">
            <div class="header menu-dark">
                <div class="container">
                    <div class="d-flex">
                        <a class="navbar-brand" href="<?php echo site_url() ?>/login">
                            <img src="<?php echo base_url()."assets/images/saturno.png"; ?>" class="navbar-brand-img" alt="tabler.io">
                            Saturno
                        </a>
                        <div class="ml-auto d-flex order-lg-2">
                            <div class="dropdown d-md-flex">
                                <a class="nav-link icon" id="alert" data-toggle="dropdown">
                                    <i class="fe fe-bell"></i>
                                    <span class="nav-unread point-alert" id="point-alert" style="display:none;"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow px-4" style="font-size: 13px;"id="mensaje-alert">
                                    <!-- Alertas -->
                                </div>
                            </div>
                            <div class="dropdown">
                                <a href="#" class="nav-link pr-0" data-toggle="dropdown">
                                    <span class="avatar-perfil avatar" style="background-image: url(<?php echo base_url()."assets/images/users/".$img; ?>)"></span>
                                        <span class="ml-2 d-none d-lg-block">
                                        <span class="text-white"><?php echo $name_user ?></span>
                                        <small class="text-muted d-block mt-1"><?php echo $perfil_label ?></small>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="javascript:void(0)" id="btn-modal-perfil">
                                        <i class="fe fe-user"></i> Mi Perfil
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" id="close-session" href="javascript:void(0)">
                                        <i class="fe fe-power"></i> Cerrar Sesión
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#header-menu">
                            <span class="header-toggler-icon"><i class="fe fe-menu"></i></span>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Modal perfil -->
            <div class="modal fade" tabindex="-1" id="modal-perfil" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><i class="fe fe-user"></i> Mi Perfil</h4>
                        </div>
                        <div class="modal-body">

                            <div class="media">
                                <span class="avatar-perfil avatar avatar-xxl mr-5" style="background-image: url(<?php echo base_url() ?>assets/images/users/<?php echo $img ?>)"></span>
                                <div class="media-body pt-4">
                                    <h4 class="m-0"><?php echo $name_user ?></h4>
                                    <p class="text-muted mb-0"><?php echo $perfil_label ?></p>
                                </div>
                            </div>

                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading mt-5" role="tab" id="heading1">
                                        <h4 class="panel-title">
                                            <a role="button" class="btn btn-primary btn-block" data-toggle="collapse" data-parent="#accordion" href="#cambio-pass" aria-expanded="true" aria-controls="imagen-perfil">
                                                Cambiar contraseña
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="cambio-pass" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                        <div class="panel-body p-2">
                                            <form id="form-cambio-pass" action="javascript:void(0)" method="post">
                                                <div class="form-group">
                                                    <label class="form-label">Contraseña actual</label>
                                                    <div class="row gutters-sm">
                                                        <div class="col">
                                                            <input type="password" class="form-control" name="passActual" id="pass" placeholder="Password actual" required>
                                                        </div>
                                                        <span show="false" class="col-auto show-pass-1 align-self-center">
                                                            <i class="show-pass-icon-1 fe fe-eye-off"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Nueva contraseña</label>
                                                    <div class="row gutters-sm">
                                                        <div class="col">
                                                            <input type="password" class="form-control" name="nuevaPass" id="nueva-pass" placeholder="Nueva password" required>
                                                        </div>
                                                        <span show="false" class="col-auto show-pass-2 align-self-center">
                                                            <i class="show-pass-icon-2 fe fe-eye-off"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <button type="submit" id="btn-bambiar-pass" class="btn btn-info m-auto">Cambiar</button>
                                        </div>
                                    </div>

                                    <div class="panel-heading mt-5" role="tab" id="heading2">
                                        <h4 class="panel-title">
                                            <a role="button" class="btn btn-primary btn-block" data-toggle="collapse" data-parent="#accordion" href="#imagen-perfil" aria-expanded="true" aria-controls="imagen-perfil">
                                                Cambiar avatar
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="imagen-perfil" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                        <div class="panel-body p-2">
                                            <div class="row">
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar1.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar1.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar2.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar2.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar3.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar3.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar4.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar4.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar5.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar5.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar6.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar6.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar7.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar7.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar8.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar8.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar9.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar9.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar10.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar10.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar11.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar11.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar12.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar12.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar13.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar13.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar14.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar14.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar15.jpg" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar15.jpg)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar16.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar16.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar17.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar17.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar18.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar18.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar19.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar19.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar20.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar20.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar21.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar21.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar22.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar22.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar23.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar23.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar24.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar24.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar25.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar25.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar26.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar26.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="avatar27.png" style="background-image: url(<?php echo base_url() ?>assets/images/users/avatar27.png)"></span>
                                                </div>
                                                <div class="col-lg-3 mt-5">
                                                    <span class="img-avatar avatar avatar-xxl mr-5" img="pony.jpg" style="background-image: url(<?php echo base_url() ?>assets/images/users/pony.jpg)"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- /.Modal perfil -->


            <!-- Modal perfil -->
            <div class="modal fade" tabindex="-1" id="loading" role="dialog">
                <div class="modal-dialog" role="document">
                    <center>
                        <img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="80px">
                    </center>
                </div>
            </div>
            <!-- /.Modal perfil -->
        
<script>

    $( function() 
    {
        notificaciones();
        setInterval('notificaciones()', 5000);
    });

    var notificaciones = function()
    {
        $.post('<?php echo site_url() ?>/notificacion/getNotificaciones')
        .done( function ( json )
        {
            json = JSON.parse(json);
            if( json.notificaciones != "") 
            {
                let alertTickets;
                if (!sessionStorage.tickets && json.tickets != false)
                {
                    sessionStorage.setItem('tickets', json.tickets);
                    tono();
                    $("#point-alert").css("display", "block");
                }
                else if (sessionStorage.tickets && json.tickets != false)
                {
                    for (let y = 0; y < json.tickets.length; y++) 
                        alertTickets = sessionStorage.tickets.indexOf(json.tickets[y]);
                    
                    if (alertTickets == -1)
                    {
                        tono();
                        $("#point-alert").css("display", "block");
                        sessionStorage.setItem('tickets', json.tickets);
                    }
                }
                else if (json.tickets == false)
                {
                    sessionStorage.setItem('tickets', json.tickets);
                }

                let alertPruebas;
                if (!sessionStorage.pruebas && json.pruebas != false)
                {
                    sessionStorage.setItem('pruebas', json.pruebas);
                    tono();
                    $("#point-alert").css("display", "block");
                }
                else if (sessionStorage.pruebas && json.pruebas != false)
                {
                    for (let y = 0; y < json.pruebas.length; y++) 
                    alertPruebas = sessionStorage.pruebas.indexOf(json.pruebas[y]);
                    
                    if (alertPruebas == -1)
                    {
                        tono();
                        $("#point-alert").css("display", "block");
                        sessionStorage.setItem('pruebas', json.pruebas);
                    }
                }
                else if (json.pruebas == false)
                {
                    sessionStorage.setItem('pruebas', json.pruebas);
                }

                let alertTareas;
                if (!sessionStorage.tareas && json.tareas != false)
                {
                    sessionStorage.setItem('tareas', json.tareas);
                    tono();
                    $("#point-alert").css("display", "block");
                }
                else if (sessionStorage.tareas && json.tareas != false)
                {
                    for (let y = 0; y < json.tareas.length; y++) 
                    alertTareas = sessionStorage.tareas.indexOf(json.tareas[y]);
                    
                    if (alertTareas == -1)
                    {
                        tono();
                        $("#point-alert").css("display", "block");
                        sessionStorage.setItem('tareas', json.tareas);
                    }
                }
                else if (json.tareas == false)
                {
                    sessionStorage.setItem('tareas', json.tareas);
                }

                $("#mensaje-alert").html(json.notificaciones); 
            } 
            else 
            {
                $("#mensaje-alert").html("<center>No hay notificaciones</center>"); 
                $("#point-alert").css("display", "none");
            }
        });
    }

    var tono = function ()
    {
        let audio = document.createElement("audio"); 
        audio.src = "<?php echo base_url() ?>assets/sound/alert.wav"; 
        audio.play();
    }

    var setStorage = function ()
    {
        $.post('<?php echo site_url() ?>/notificacion/getNotificaciones')
        .done( function ( json )
        {
            json = JSON.parse( json );

            sessionStorage.setItem('tareas', json.tareas);
            sessionStorage.setItem('pruebas', json.pruebas);
            sessionStorage.setItem('tickets', json.tickets);

            console.log(json);
        });
    }

    $("#btn-modal-perfil").click( function ()
    {
        $("#modal-perfil").modal();
    });

    $(".img-avatar").click( function ()
    {
        let img = $(this).attr("img");
        $.post( "<?php echo site_url() ?>/usuario/saveAvatar", {img:img} ).
        done( function ( json )
        {
            json = JSON.parse( json );
            if ( json.response ) 
            {
                toast({ type: 'success', title: 'Avatar guardado.' });
                $(".avatar-perfil").css('background-image', 'url(<?php echo base_url() ?>assets/images/users/'+img+')');
            }
            else
            {
                toast({ type: 'error', title: 'Error al guardar.' });
            }
        });
    });

    $(".show-pass-1").click( function ()
    {
        $(this).find(".show-pass-icon-1").toggleClass('fe-eye-off');
        $(this).find(".show-pass-icon-1").toggleClass('fe-eye');

        if($("#pass").attr("type") == "password")
            $("#pass").attr("type", "text");
        else 
            $("#pass").attr("type", "password");
    });

    $(".show-pass-2").click( function ()
    {
        $(this).find(".show-pass-icon-2").toggleClass('fe-eye-off');
        $(this).find(".show-pass-icon-2").toggleClass('fe-eye');

        if($("#nueva-pass").attr("type") == "password")
            $("#nueva-pass").attr("type", "text");
        else 
            $("#nueva-pass").attr("type", "password");
    });

    $("#btn-bambiar-pass").click( function ()
    {
        if (validaForm($("#form-cambio-pass")))
        {
            let data = $("#form-cambio-pass").serialize();
            $.post( "<?php echo site_url() ?>/usuario/changePass", data )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if( json.passActual )
                    if( json.save > 0)
                    {
                        toast({ type: 'success', title: 'Contraseña actualizada.' });
                        setTimeout( function()
                        {
                            window.location.href = "<?php echo site_url() ?>/login/log_out";
                        }, 1500);
                    }
                    else
                    {
                        toast({ type: 'error', title: 'Error al actualizar contraseña.' });
                    }
                else
                {
                    toast({ type: 'error', title: 'La contraseña actual no coincide.' });
                }
                
                $("#pass").val("");
                $("#nueva-pass").val("");
            });
        }
    });

    $("#close-session").click( function ()
    {
        sessionStorage.clear();
        window.location.href = "<?php echo site_url() ?>/login/log_out";
    });

</script>