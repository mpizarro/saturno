
<div class="header menu-dark d-lg-flex p-0 collapse" id="header-menu" style="">
    <div class="container">
        <div class="row align-items-center">
            <div class="ml-auto mr-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/departamento/tickets"; ?>" class="nav-link <?php echo $menu[ 'tickets' ] ?>"><i class="fe fe-tag"></i> tickets</a>
                    </li> 
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/departamento/historial"; ?>" class="nav-link <?php echo $menu[ 'historial_tickets' ] ?>"><i class="fe fe-clock"></i> Historial</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>