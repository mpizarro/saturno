

<div class="page-content">
    <div class="container">

        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div style="width:100%">
                        <div style="float:left">
                            <h3 class="card-title">Usuarios</h3>
                        </div>
                        <div style="float:right">
                            <button id="btn-modal-create-user" type="button" class="btn btn-info btn-new-user"><i class='fe fe-user-plus'></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-body row pt-0 table-responsive">

                    <table id="tabla-usuarios" class="table card-table table-vcenter text-nowrap col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <thead>
                            <tr>
                                <th class="w-1">No. Id</th>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Perfil</th>
                                <th>Categoria</th>
                                <th>Cumplimiento %</th>
                                <th>Puntaje total</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal update user -->
<div class="modal" tabindex="-1" role="dialog" id="modal-update-user">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-update-user">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Nombre</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="nombre" id="nombre-update" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Usuario</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="usuario" id="usuario-update" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Departamentos</label>
                            <div class="input-icon mb-3">
                                <select  name="departamento" id="departamento-update" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Perfil</label>
                            <div class="input-icon mb-3">
                                <select  name="perfil" id="perfil-update" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Testing</label>
                            <div class="input-icon mb-3">
                                <select  name="perfil" id="is-testing-update" class="form-control custom-select" required>
                                    <option value="0" hidden>Selecciona...</option>
                                    <option value="t">SI</option>
                                    <option value="f">NO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-update-user" type="submit" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal update user -->


<!-- modal create user -->
<div class="modal" tabindex="-1" role="dialog" id="modal-create-user">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-create-user">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Nombre</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="nombre" id="nombre" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Usuario</label>
                            <div class="input-icon mb-3">
                                <input type="text" name="usuario" id="usuario" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-label">Contraseña</label>
                            <div class="row gutters-sm">
                                <div class="col">
                                <input type="password" name="password" id="password" class="form-control" ||>
                                </div>
                                <span id="show-pass" show="false" class="col-auto align-self-center">
                                    <i class="show-pass fe fe-eye-off"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Departamentos</label>
                            <div class="input-icon mb-3">
                                <select  name="departamento" id="departamento" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Perfil</label>
                            <div class="input-icon mb-3">
                                <select  name="perfil" id="perfil" class="form-control custom-select" required>
                                
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label class="form-label">Testing</label>
                            <div class="input-icon mb-3">
                                <select  name="isTesting" id="is-testing" class="form-control custom-select" required>
                                    <option value="0" hidden>Selecciona...</option>
                                    <option value="t">SI</option>
                                    <option value="f">NO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-create-user" type="submit" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal create user -->

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript">

    var user;

    $( document ).ready( function()
    {
        getUsuarios();
        selectPerfil();
        selectDepartamento();
    });

    $(document).on("click", ".btn-modal-update-user", function ()
    {
        user = $(this).attr("id-usuario");
        $.post( '<?php echo site_url() ?>/usuario/getUsuario', {user:user} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#nombre-update").val( json.nombre );
            $("#usuario-update").val( json.usuario );
            $("#perfil-update option[value="+json.perfil+"]").attr("selected", true);
            $("#is-testing-update").val( json.testing );
            $("#departamento-update option[value="+json.departamento_id+"]").attr("selected", true);
        });
        $("#modal-update-user").modal();
    });

    $("#btn-update-user").click( function()
    {
        if( validaForm( $("#form-update-user") ) )
        {
            let data = {
                'nombre': $("#nombre-update").val(),
                'usuario': $("#usuario-update").val(),
                'perfil': $("#perfil-update").val(),
                'departamento': $("#departamento-update").val(),
                'isTesting': $("#is-testing-update").val()
            }
            console.log(data);
            $.post( '<?php echo site_url() ?>/usuario/updateUser/'+user, data )
            .done( function ( json ) 
            {
                json = JSON.parse( json );
                if ( json.resp > 0 )
                    toast({type:'success', title:'¡Perfecto.! Registro actualizado.'});
                else
                    toast({type:'error', title:'¡Error.! No se logro realizar esta acción.'});
                $("#modal-update-user").modal('hide');
                getUsuarios();
            });
        }
    });

    $("#btn-modal-create-user").click( () => {
        $("#modal-create-user").modal();
    });

    $("#btn-create-user").click( function ()
    {
        if ( validaForm( $("#form-create-user") ) )
        {
            let data = $("#form-create-user").serialize();
            $.post( '<?php echo site_url() ?>/usuario/saveUser', data )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.resp )
                    toast({type:'success', title:'¡Perfecto.! Usuario guardado.'});
                else
                    toast({type:'error', title:'¡Error.! No se logro realizar esta acción.'});
                $("#modal-create-user").modal('hide');
                getUsuarios();
            });
        }
    });

    $(document).on("click", ".btn-modal-delet-user", function ()
    {
        let user = $(this).attr("id-usuario");
        swal({
            title: '¿Estás seguro?',
            text: "¡El registro se eliminara permanentemente!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar!'
        })
        .then((result) => {
            if (result.value) 
            {
                $.post('<?php echo site_url() ?>/usuario/deleteUser', { user:user} )
                .done( function ( json )
                {
                    json = JSON.parse( json );
                    if(json.resp > 0) 
                        toast({type:'success', title:'¡Perfecto.! Registro eliminado.'});
                    else
                        toast({type:'error', title:'¡Error.! No se logro realizar esta acción.'});
                    getUsuarios();
                });
            }
        });
    })

    var getUsuarios = function()
    {
        $("#tabla-usuarios").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 10,
            "ajax" : {
                "url" : "<?php echo site_url() ?>/usuario/getUsuarios",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'usuario'},
                {data: 'usuario2'},
                {data: 'perfil'},
                {data: 'categoriaAvatar'},
                {data: 'porcentaje'},
                {data: 'puntaje'},
                {data: 'acciones'}
            ]
        });
        $("#tabla-usuarios_filter").remove();
    }

    var selectPerfil = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/usuario/selectPerfil",
            success: function ( json )
            {
                json = JSON.parse( json );
                $("#perfil-update").html( json.select );
                $("#perfil").html( json.select );
            }
        });
    }

    var selectDepartamento = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/usuario/selectDepartamento",
            success: function ( json )
            {
                json = JSON.parse( json );
                $("#departamento-update").html( json.select );
                $("#departamento").html( json.select );
            }
        });
    }

    $("#show-pass").click( function ()
    {
        $(".show-pass").toggleClass('fe-eye-off');
        $(".show-pass").toggleClass('fe-eye');

        if($("#password").attr("type") == "password")
            $("#password").attr("type", "text");
        else 
            $("#password").attr("type", "password");
    });
</script>