
<div class="header menu-dark d-lg-flex p-0 collapse" id="header-menu" style="">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="ml-auto mr-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/pizarra"; ?>" class="nav-link <?php echo $menu[ 'pizarra' ] ?>"><i class="fe fe-calendar"></i> Pizarra Tareas</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/testing"; ?>" class="nav-link <?php echo $menu[ 'testing' ] ?>"><i class="fe fe-activity"></i> Testing</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/backlogs"; ?>" class="nav-link <?php echo $menu[ 'backlogs' ] ?>"><i class="fe fe-server"></i> Backlogs</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/proyectos"; ?>" class="nav-link <?php echo $menu[ 'proyectos' ] ?>"><i class="fe fe-box"></i> Proyectos</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/tickets"; ?>" class="nav-link <?php echo $menu[ 'tickets' ] ?>"><i class="fe fe-tag"></i> tickets</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/usuarios"; ?>" class="nav-link <?php echo $menu[ 'usuarios' ] ?>"><i class="fe fe-users"></i> Usuarios</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/historial_tareas"; ?>" class="nav-link <?php echo $menu[ 'historial_tareas' ] ?>"><i class="fe fe-clock"></i> Historial Tareas</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/administrador/historial_tickets"; ?>" class="nav-link <?php echo $menu[ 'historial_tickets' ] ?>"><i class="fe fe-clock"></i> Historial Tickets</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>