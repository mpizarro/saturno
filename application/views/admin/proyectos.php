
<div class="page-content">
    <div class="container">
        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div style="width:100%">
                        <h3 class="card-title" style="float:left">Proyectos</h3>
                        <button id="crear_proyecto" type="button" style="float:right" class="btn btn-info btn-new-user"><i class='fe fe-plus-square'></i></button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="tabla-proyectos" class="table card-table table-vcenter text-nowrap">
                        <thead>
                            <tr>
                                <th class="w-1">No. Id</th>
                                <th>Proyecto</th>
                                <th>Código</th>
                                <th>Creación</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>           
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal-crear-proyecto">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo Proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="form-crear-proyecto">
                    <div class="form-group">
                        <label class="form-label">Nombre</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Ingresa el nombre de tu proyecto..." required>
                    </div>
                    <div class="form-group mb-0">
                        <label class="form-label">Código</label>
                        <input type="text" id="codigo" name="codigo" class="form-control" placeholder="Ingresa un código o alias para tu proyecto..." required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn_crear_proyecto" type="submit" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal-update-proyecto">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar Proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <label class="form-label">Nombre</label>
                        <input type="text" id="name-update" name="name" class="form-control" placeholder="Ingresa el nombre de tu proyecto..." required>
                    </div>
            
                    <div class="form-group mb-0">
                        <label class="form-label">Código</label>
                        <input type="text" id="codigo-update" name="codigo" class="form-control" placeholder="Ingresa un código o alias para tu proyecto..." required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btn-update-proyecto" type="submit" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>





<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript">

    var id_proyecto = 0;

    $(document).ready( function() 
    {
        getProyectos();
    })

    $("#crear_proyecto").click( function () 
    {
        $("#name").val("");
        $("#codigo").val("");
        $("#modal-crear-proyecto").modal();
    });

    $(document).on("click", ".remove-proyecto",  function()
    {
        id_proyecto = $(this).attr("id-proyecto");
        backlog = $(this).attr("backlog_id");
        swal({
            title: '¿Estás seguro?',
            text: "¡El registro se eliminara permanentemente!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar!'
        })
        .then((result) => {
            if (result.value) {
                $.post('<?php echo site_url() ?>/proyecto/remove_proyecto', { id_proyecto:id_proyecto})
                .done( function ( json )
                {
                    json = JSON.parse(json);
                    if(json.resp)
                        swal('¡Perfecto!', 'Tu proyecto se elimino con éxito.', 'success');
                    else
                        swal('¡Error!', 'No se logro realizar esta acción.', 'error');
                    
                    getProyectos();
                });
            }
        });
    });

    $(document).on("click", ".update-proyecto",  function()
    {
        id_proyecto = $(this).attr("id-proyecto");
        $.post( "<?php echo site_url() ?>/proyecto/getProyecto", {id_proyecto:id_proyecto} )
        .done( function ( json ) 
        {
            json = JSON.parse( json );
            $("#name-update").val( json.nombre);
            $("#codigo-update").val( json.codigo);
        })
        $("#modal-update-proyecto").modal();
    });
    
    $("#btn-update-proyecto").click( function ()
    {
        if ( $("#name-update").val() != "" && $("#codigo-update").val() != "")
        {
            let data = {
                'name': $("#name-update").val(),
                'codigo': $("#codigo-update").val()
            };
            $.post( "<?php echo site_url() ?>/proyecto/update_proyecto/"+id_proyecto, data )
            .done( function ( json )
            {
                json = JSON.parse( json );
                if ( json.response ) {
                    swal({
                        title: '¡Perfecto!',
                        text: "El registro se actualizo correctamente.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    })
                    .then((result) => 
                    {
                        $("#modal-update-proyecto").modal("hide");
                        getProyectos();
                    });
                }
            });
        }
    });

    $("#btn_crear_proyecto").click( function ()
    {
        if ( $("#name").val() != "" && $("#codigo").val() != "")
        {
            let data = {
                'name': $("#name").val(),
                'codigo': $("#codigo").val()
            };
            $.post ( "<?php echo site_url() ?>/proyecto/save_proyecto",  data )
            .done ( function ( response ) 
            {
                let json = JSON.parse( response );
                if( json.resp )
                    swal('¡Perfecto!', 'Tu proyecto se guardo con éxito.', 'success');
                else
                    swal('¡Error!', 'Ocurrió un error al guardar tu proyecto', 'error');
                
                $("#modal-crear-proyecto").modal("hide");
                getProyectos();
            });
        }
    });

    var getProyectos = function()
    {
        $("#tabla-proyectos").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 15,
            "ajax" : {
                "url" : "<?php echo site_url() ?>/proyecto/getProyectos",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'descripcion'},
                {data: 'codigo'},
                {data: 'fecha'},
                {data: 'acciones'}
            ]
        });
        $("#tabla-proyectos_filter").remove();
    }

</script>