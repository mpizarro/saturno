
<!-- cambiar -->


<div class="page-content">
    <div class="container">
            
        <div class="col-lg-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div style="width:100%">
                        <div style="float:left">
                            <h3 class="card-title">Backlog</h3>
                        </div>
                        <div style="float:right">
                            <button id="crear_backlog" type="button" class="btn btn-info btn-new-backlog"><i class="fe fe-plus-square"></i></button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap" id="tabla-backlog">
                        <thead>
                            <tr>
                                <th class="w-1">No. Id</th>
                                <th>Asunto</th>
                                <th>Proyecto</th>
                                <th>Código</th>
                                <th>Informador</th>
                                <th>Creado</th>
                                <th style="text-align: center;">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    
    
       
<!-- modals -->
<div class="modal" tabindex="-1" role="dialog" id="mdl_asignarBacklog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar Responsable</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label class="form-label">Lista de usuarios</label>
                    <select name="user" id="selectUsuarios" class="form-control custom-select">
                    
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Prioridad</label>
                    <select name="user" id="select-prioridad" class="form-control custom-select">
                        <option value="0" hidden>Selccione...</option>
                        <option value="1">Baja</option>
                        <option value="2">Media</option>
                        <option value="3">Alta</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Horas estimadas</label>
                    <input type="text" placeholder="00:00" name="horas" id="horas" data-mask="99:99" class="form-control">
                </div>
                <div class="form-group">
                    <label class="form-label">Dificultad</label>
                    <select name="user" id="selectDificultad" class="form-control custom-select">
                        <?php echo $dificultad ?>
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="btnAsignarResponsable" type="button" class="btn btn-info">Asignar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="mdl_creaBacklog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crear Backlog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body"> 
                <form id="frm_backlog" action="javascript:void(0)" method="post">
                    <div class="form-group">
                        <label class="form-label">Asunto</label>
                        <input type="text" name="asunto" id="asunto" class="form-control">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Proyecto</label>
                        <select name="proyecto" id="selectProyectos" class="form-control custom-select">
                            
                        </select>
                    </div>
                
                    <div class="form-group">
                        <label class="form-label">Descripción</label>
                        <textarea name="descripcion" id="descripcion" rows="5" class="form-control" placeholder="Descripción Backlog" value="Mike"></textarea>
                    </div>

                    <div class="form-group mb-0">
                        <label class="form-label">Adjuntos</label>
                        <input type="file" name="adjunto" id="adjunto" class="form-control" multiple>
                    </div>

                    <input type="hidden" name="usuario_id" id="usuario_id" value="<?php echo $id_usuario ?>">
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="save_backlog" type="button" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="mdl_editarBacklog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Backlog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body"> 
                <form id="frm_edit_backlog" action="javascript:void(0)" method="post">
                    <div class="form-group">
                        <label class="form-label">Asunto</label>
                        <input type="text" name="asunto" id="asunto-update" class="form-control">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Proyecto</label>
                        <select name="proyecto" id="selectProyectos-update" class="form-control custom-select">
                            
                        </select>
                    </div>
                
                    <div class="form-group">
                        <label class="form-label">Descripción</label>
                        <textarea name="descripcion" id="descripcion-update" rows="5" class="form-control" placeholder="Descripción Backlog" value="Mike"></textarea>
                    </div>

                    <div class="form-group">
                        <label class="form-label">Adjuntos</label>
                        <input type="file" name="adjunto" id="adjunto-update" class="form-control" multiple>
                    </div>

                    <div class="form-group mb-0">
                        <label class="form-label">Archivos</label>
                        <div class="content-adjuntos row">
                            <!-- adjuntos -->
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                <button id="editar_backlog" type="button" class="btn btn-info">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal-descripcion" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h4 class="modal-title"><i class="fe fe-list"></i> Descripcion</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body d-flex flex-column">
                    <center><h4><a href="javascript:void(0)" id="nameTarea"></a></h4></center>
                    <div class="d-flex align-items-center mb-3">
                        <div class="row">
                            <div class="col-12">Asunto: <a href="javascript:void(0)" class="text-muted ml-2" id="asunto-des"></a></div>
                            <div class="col-12">Codigo: <a href="javascript:void(0)" class="text-muted ml-2" id="codigo-des"></a></div>
                        </div>
                        <div class="ml-auto" id="estado">
                            <div class="col-12">Proyecto: <a href="javascript:void(0)" class="text-muted ml-2" id="proyecto-des"></a></div>
                        </div>
                    </div>
                    <label>Descripción:</label>
                    <div class="text-muted" id="descripcion-des"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

   
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/mask.js"></script>
<script type="text/javascript">

    var backlog = 0;

    $(document).ready( function ()
    {
        getBacklogs();
        getSelectProyectos();
        getSelectUsuarios();
        getSelectDificultad();
    });

    $(document).on('click', '[download-file]', function()
    {
        window.location.href = `<?= site_url() ?>/backlog/downloadFile?nameFile=${$(this).attr('file')}`;

    });

    $(document).on('click', '[btn-delete-adjunto]', function() 
    {
        let idAdjunto = $(this).attr("id-adjunto");
        console.log(idAdjunto);
        $.post('<?= site_url() ?>/backlog/eliminarAdjunto', {id:idAdjunto})
        .done( json => {
            json = JSON.parse(json);
            if (json.response) {
                toast({type:'success', text:'Archivo Eliminado!'});
                $(document).find(`[cont-adjunto='cont-adj-${idAdjunto}']`).remove();
            } else {
                toast({type:'error', text:'No se pudo eliminar!'});
            }
        });
    });

    $(document).on("click", ".btnModalAsignarBacklog", function()
    {
        backlog = $(this).attr('backlog_id');

        $("#selectUsuarios").val("0");
        $("#select-prioridad").val("0");
        $("#selectDificultad").val("0");
        $("#horas").val("");

        $('#mdl_asignarBacklog').modal('show');  
    });

    $(document).on("click", ".btnDetalleBacklog", function ()
    {
        backlog = $(this).attr("backlog_id");
        $.post( '<?php echo site_url() ?>/backlog/getDetalle', {backlog:backlog} )
        .done( function (json )
        {
            json = JSON.parse( json );
            $("#asunto-des").html(json.asunto);
            $("#codigo-des").html(json.codigo);
            $("#proyecto-des").html(json.proyecto);
            $("#descripcion-des").html(json.detalle);
            $("#modal-descripcion").modal();
        });
    });

    $(document).on("click", ".btnModalUpdateBacklog", function ()
    {
        backlog = $(this).attr("backlog_id")
        $.post( "<?php echo site_url() ?>/backlog/getBacklog", {id:backlog} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#asunto-update").val( json.asunto );
            $("#selectProyectos-update").val( json.proyecto ).change();
            $("#descripcion-update").val( json.descripcion );
            $(".content-adjuntos").html(json.adjuntos);
        });
        $("#mdl_editarBacklog").modal();
    });

    $("#crear_backlog").click(function()
    {
        $("#asunto").val(null);
        $("#selectProyectos").val(0);
        $("#descripcion").val(null);

        $('#mdl_creaBacklog').modal('show');  
    });

    $("#btnAsignarResponsable").click(function()
    {
        var usuario = $("#selectUsuarios").val();
        var prioridad = $("#select-prioridad").val();
        var dificultad = $("#selectDificultad").val();
        var horas = $("#horas").val();
        if (usuario != "" && prioridad != "" && dificultad != "" && horas != "")
        {
            $.post( '<?php echo site_url()."/backlog/asignatarea"; ?>', { usuario:usuario, backlog:backlog, prioridad:prioridad, dificultad:dificultad, horas:horas })
            .done(function( data ) 
            {
                var json = eval("(" + data + ")");
                if(json.success)
                    toast({ type: 'success', title: '¡Backlog asignado!' });
                else
                    toast({ type: 'error', title: '¡Error!' });
                $('#mdl_asignarBacklog').modal('hide');
                getBacklogs();
            });
        }
    });

    $("#save_backlog").click(function()
    {
        let inputFile = document.getElementById('adjunto');
        let data = new FormData();

        for (let i = 0; i < inputFile.files.length; i++)
            data.append(i, inputFile.files[i]);

        data.append('asunto', $("#asunto").val());
        data.append('proyecto', $("#selectProyectos").val());
        data.append('descripcion', $("#descripcion").val());
        data.append('usuario_id', $("#usuario_id").val());

        $.ajax({
            url:'<?php echo site_url()."/backlog/save_backlog"; ?>',
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(json)
            { 
                json = JSON.parse( json );
                if (json.response == "success") {
                    toast({type:'success', title:'Backlog creado'});
                    if (!json.response_file)
                        toast({type:'error', title:'No se logro guardar el archvo adjunto'});

                } else { 
                    toast({type:'error', title:'Error al crear'});
                }
                location.reload();
            } 
        });    
    });

    $("#editar_backlog").click(function()
    {
        if ( $("#asunto-update").val() != "" && $("#selectProyectos-update").val() != 0 && $("#descripcion-update").val() ) {
            let inputFile = document.getElementById('adjunto-update');
            let data = new FormData();

            for (let i = 0; i < inputFile.files.length; i++)
                data.append(i, inputFile.files[i]);

            data.append('asunto', $("#asunto-update").val());
            data.append('proyecto', $("#selectProyectos-update").val());
            data.append('descripcion', $("#descripcion-update").val());
            data.append('id', backlog);

            $.ajax({
                url:'<?php echo site_url()."/backlog/update"; ?>',
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(json)
                { 
                    json = JSON.parse( json );
                    if (json.response) {
                        toast({type:'success', title:'Backlog creado'});
                        if (!json.response_file)
                            toast({type:'error', title:'No se logro guardar el archvo adjunto'});

                    } else { 
                        toast({type:'error', title:'Error al crear'});
                    }
                    location.reload();
                } 
            }); 
        }   
    });

    $(document).on("click", ".btnEliminarBacklog", function ()
    {
        backlog = $(this).attr("backlog_id");
        swal({
            title: '¿Estás seguro?',
            text: "¡El registro se eliminara permanentemente!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar!'
        })
        .then((result) => {
            if (result.value) {
                $.post('<?php echo site_url() ?>/backlog/removeBacklog', { backlog:backlog})
                .done( function ( json )
                {
                    json = JSON.parse(json);
                    console.log(json.resp);
                    if(json.resp)
                        toast({type:'success', title:'Backlog Eliminado'});
                    else
                        toast({type:'error', title:'Error al eliminar'});
                    getBacklogs();
                });
            }
        });
    });

    var getSelectProyectos = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/proyecto/selectProyectos",
            success: function (json )
            {
                json = JSON.parse( json );
                $("#selectProyectos").html( json.select );
                $("#selectProyectos-update").html( json.select );
            }
        });
    }

    var getSelectUsuarios = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/usuario/selectUsuarios",
            success: function (json )
            {
                json = JSON.parse( json );
                $("#selectUsuarios").html( json.select );
            }
        });
    }

    var getSelectDificultad = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/backlog/selectDificultad",
            success: function (json )
            {
                json = JSON.parse( json );
                $("#selectDificultad").html( json.select );
            }
        });
    }

    var getBacklogs = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/backlog/getBacklogs",
            success: function (json )
            {
                json = JSON.parse( json );
                $("#rowsBacklogs").html( json.backlogs );
            }
        });

        $("#tabla-backlog").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 10,
            "order" : [[0, 'desc']],
            "ajax" : {
                "url" : "<?php echo site_url() ?>/backlog/getBacklogs",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'asunto'},
                {data: 'descripcion'},
                {data: 'codigo'},
                {data: 'informador'},
                {data: 'fecha'},
                {data: 'acciones'}
            ]
        });
        $("#tabla-backlog_filter").remove();
    }

</script>