
<div class="page-content">
    <div class="container">
        <div class="col-lg-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div style="width:100%">
                        <h3 class="card-title" style="float:left">Historial Tareas</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="tabla-historial" class="table card-table table-center text-nowrap">
                        <thead>
                            <tr>
                                <th># ID</th>
                                <th>Informante</th>
                                <th>Tarea</th>
                                <th>Estado Actual</th>
                                <th>Prioridad</th>
                                <th>Historial</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>           
</div>


<!-- Modal pruebas cruzadas -->
<div class="modal fade" tabindex="-1" id="modal-historial" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fe fe-clock"></i> Historial</h4>
            </div>
            <div class="modal-body">

                <div class="time-line">
                    <div class="content-time-line" id="line_time">

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" id="modal-descripcion" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h4 class="modal-title"><i class="fe fe-list"></i> Descripcion</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body d-flex flex-column">
                    <center><h4><a href="javascript:void(0)" id="nameTarea"></a></h4></center>
                    <div class="d-flex align-items-center mb-3">
                        <div class="row">
                            <div class="col-12">Prioridad: <a href="javascript:void(0)" class="text-muted ml-2" id="prioridad"></a></div>
                            <div class="col-12">Inicio: <a href="javascript:void(0)" class="text-muted ml-2" id="fecha"></a></div>
                        </div>
                        <div class="ml-auto text-muted" id="estado"></div>
                    </div>
                    <label>Descripción:</label>
                    <div class="text-muted" id="descripcion"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- /.Modal pruebas cruzadas -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script>
    $(document).ready( function()
    {
        getTablaTareas();
    });

    $(document).on("click", ".btn-historial", function() 
    {
        let tarea = $(this).attr('id-tarea');
        $.post( '<?php echo site_url() ?>/historial/getLineTimeTareas', {tarea:tarea})
        .done( function ( json  ) {
            json = JSON.parse( json );
            $("#line_time").html( json.line );
        });

        $("#modal-historial").modal();
    });

    let getTablaTareas = function()
    {
        $("#tabla-historial").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 10,
            "order" : [[0, 'desc']],
            "ajax" : {
                "url" : "<?php echo site_url() ?>/tarea/getTablaTareas",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'informante'},
                {data: 'codigo'},
                {data: 'estado'},
                {data: 'prioridad'},
                {data: 'acciones'}
            ]
        });
        $("#tabla-historial_filter").remove();
    }

    $(document).on("click", ".detalle-tarea", function ()
    {
        tarea = $(this).attr("id-tarea");
        $.post( '<?php echo site_url() ?>/tarea/getDescripcion', {tarea:tarea})
        .done( function ( response ) 
        {
            let json = JSON.parse( response );

            $("#estado").html(json.estado);
            $("#prioridad").html(json.prioridad);
            $("#fecha").html(json.fecha);
            $("#nameTarea").html(json.tarea);
            $("#descripcion").html(json.descripcion);

            $("#modal-descripcion").modal();
        });
    });
</script>