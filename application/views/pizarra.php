
<div class="page-content">
    <div class="container">

        <div class="row">

            <div class="card card-dark col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tareas" class="nav-link active show" aria-controls="tareas" role="tab" data-toggle="tab" aria-selected="true">Tareas</a></li>
                    <li role="presentation"><a href="#merged" id="tareas-merged" class="nav-link" aria-controls="merged" role="tab" data-toggle="tab">Merged</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tareas">
                        <div class="card-body row">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-blue"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">Asignadas <span id="n1" class="badge bg-blue"></span></div>
                                        <div class="column scroll-column" id="1" name-estado="Asignada">
                                            <!-- tareas asignadas --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-orange"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">En desarrollo <span id="n2" class="badge bg-orange"></span></div>
                                        <div class="column scroll-column" id="2" name-estado="en Desarrollo">
                                            <!-- tareas en desarrollo --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-green"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">Entregadas <span id=n3 class="badge bg-green"></span></div>
                                        <div class="column scroll-column" id="3" name-estado="Entregada">
                                            <!-- tareas entregadas --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-yellow"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">Testing <span id="n4" class="badge bg-yellow"></span></div>
                                        <div class="column scroll-column" id="4" name-estado="en Testing">
                                            <!-- tareas en testing --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-teal"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">Finalizadas <span id="n5" class="badge bg-teal"></span></div>
                                        <div class="column scroll-column" id="5" name-estado="Finalizada">
                                            <!-- tareas finalizadas --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                <div class="card card-estados">
                                    <div class="card-status bg-purple"></div>
                                    <div class="card-body text-center">
                                        <div class="card-category">Merged <span id="n6" class="badge bg-purple"></span></div>
                                        <div class="column scroll-column" id="6" name-estado="Merged">
                                            <!-- tareas merged --><center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="merged">
                        <div class="card-body pt-0 table-responsive">
                            <table class="table card-table table-striped" id="table-merged" style="width:100%!important;">
                                <thead>
                                    <tr>
                                        <th># ID</th>
                                        <th>Informante</th>
                                        <th>Tarea</th>
                                        <th>Prioridad</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
    </div>
</div>


<div class="modal fade" tabindex="-1" id="modal-user-random" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-random"></i> Usuarios</h4>
            </div>
            <div class="modal-body">
                <label class="text-muted">Asignando prueba cruzada a: </label>
                <div class="progress" style="height:3rem;">
                    <div class="progress-bar" id="progreso" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 40%; padding: 1.8rem;">
                        <h4 class="cuntent-user-random" id="usuarios"></h4>
                    </div>
                </div>
                <p class="text-muted">Al usuario seleccionado se le notificara acerca de esta prueba.</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-random" class="btn btn-warning">Otro</button>
                <button type="button" id="btn-asignar-tc" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" id="modal-descripcion" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h4 class="modal-title"><i class="fe fe-list"></i> Descripcion</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body d-flex flex-column">
                    <center><h4><a href="javascript:void(0)" id="nameTarea"></a></h4></center>
                    <div class="d-flex align-items-center mb-3">
                        <div class="row">
                            <div class="col-12">Prioridad: <a href="javascript:void(0)" class="text-muted ml-2" id="prioridad"></a></div>
                            <div class="col-12">Inicio: <a href="javascript:void(0)" class="text-muted ml-2" id="fecha"></a></div>
                        </div>
                        <div class="ml-auto text-muted" id="estado"></div>
                    </div>
                    <label>Descripción:</label>
                    <div class="text-muted" id="descripcion"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" id="modal-observaciones" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <h4 class="modal-title"><i class="fe fe-list"></i> Observación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body d-flex flex-column">
                    <label>Observacion:</label>
                    <div class="text-muted" id="observacion"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript">

    var tarea = 0;
    var solicitado = 0;
    var nombreUsuario = "";

    $(document).ready( function ()
    {
        getTareas();
    });

    $('.column').sortable(
    {
        receive: function( event, ui ) 
        {
            tarea = ui.item.attr("id");
            let estado = $(this).attr("id");
            let labelEstado = $(this).attr("name-estado");
            let estadoAnterior = ui.sender.attr("id");
            let usuario = ui.item.attr("id-usuario");

            $.post( "<?php echo site_url() ?>/tarea/getEstados", {tarea:tarea})
            .done( function ( json )
            {
                json = JSON.parse(json);

                if (json.estadoPrueba == 11) 
                {
                    $(".column").sortable("cancel");
                    toast({ type: 'warning', title: 'La tarea esta en testing' });
                    getTareas();
                }
                else if (json.estadoPrueba == 13) 
                {
                    if ( estado != 2 ) {
                        $(".column").sortable("cancel");
                        toast({ type: 'warning', title: 'La tarea esta defectuosa, se sugiere llevar a desarrollo.' });
                        getTareas();
                    }
                    else
                    {
                        $.post( "<?php echo site_url() ?>/pruebaCruzada/delete", {tarea:tarea});
                        changeEstado(tarea, estado, usuario, labelEstado); 
                    }
                }
                else if ( estadoAnterior == 2 && json.tareaDetenida == 't')
                {
                    $(".column").sortable("cancel");
                    toast({ type: 'warning', title: 'La tarea se encuantra detenida.' });
                    getTareas();
                }
                else
                {
                    if ( estado == 4 ) 
                    {
                        $('#modal-user-random').modal({backdrop: 'static', keyboard: false})
                        usuarioRandom()
                    }
                    else
                    {
                        $.post( "<?php echo site_url() ?>/pruebaCruzada/delete", {tarea:tarea});
                    }

                    if (estadoAnterior == 2)
                    {
                        $.post("<?php echo site_url() ?>/tarea/correrPausar", {tarea:tarea, estado:'f'});
                    }
                    else if (estado == 2)
                    {
                        $.post("<?php echo site_url() ?>/tarea/correrPausar", {tarea:tarea, estado:'t'});
                    }

                    changeEstado(tarea, estado, usuario, labelEstado);                
                }
            });
        }
    });

    $("#btn-random").click( function ()
    {
        usuarioRandom();
    });

    var usuarioRandom = function(usuarios)
    {
        $.ajax({
            url: '<?php echo site_url() ?>/usuario/getArrayUsers',
            success: function ( json )
            {
                json = JSON.parse(json);
                let usuarios = json.usuarios;
                if(usuarios != false) 
                {
                    let max = usuarios['nombre'].length;

                    var limit = 0;
                    var index = 0;

                    let intervalo = setInterval( function()
                    {
                        if(limit <= 10) 
                        {
                            $("#btn-asignar-tc").attr('disabled', true);
                            $("#progreso").css("width", (limit * 10)+"%");
                            index = Math.floor(Math.random() * (max - 0)) + 0;
                            let imagen = "<span class='avatar mr-4' style='background-image: url(<?php echo base_url() ?>/assets/images/users/"+usuarios['imagen'][index]+")'></span>";
                            $("#usuarios").html( imagen + usuarios['nombre'][index]);
                            solicitado = usuarios['id'][index];
                            nombreUsuario = usuarios['nombre'][index];
                            limit++;
                        } 
                        else 
                        {
                            clearInterval(intervalo);
                            $("#btn-asignar-tc").attr('disabled', false);
                        }
                    }, 200);
                } 
                else 
                {
                    swal("¡Error!", "No existen usuarios", "error");
                }
            }
        });
    }

    $("#btn-asignar-tc").click( function () 
    {
        $.post( '<?php echo site_url() ?>/PruebaCruzada/asignarPruebaCruzada', {'solicitado':solicitado, 'tarea':tarea} )
        .done( function ( json )
        {
            json = JSON.parse(json);
            if (json.resp > 0) {
                toast({ type: 'success', title:nombreUsuario!=""?"Solicitud enviada a "+nombreUsuario:"Solicitud enviada" });
            } else {
                toast({ type: 'error', title: 'Error al enviar solicitud' });
            }
            getTareas();
        });
    });

    $(".nav-tabs").find('a').click( function () 
    {
        $(".nav-tabs").find('li').removeClass("active");
        $(this).parent('li').addClass("active");
    });

    var changeEstado = function(tarea, estado, usuario, labelEstado) 
    {
        $.post( '<?php echo site_url() ?>/tarea/change_estado', {'tarea':tarea, 'estado':estado, 'usuario':usuario} )
        .done( ( json ) => {
            json = JSON.parse( json );
            if( json.success )
                toast({ type: 'success', title: 'Tarea ' + labelEstado });
            else
                toast({ type: 'error', title: 'Error al cambiar el estado.' });
            getTareas();
        });
    }

    $(document).on("click", ".remove-tarea", function ()
    {
        tarea = $(this).attr("id-tarea");
        swal({
            title: '¿Estás seguro?',
            text: "¡El registro se eliminara permanentemente!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar!'
        })
        .then((result) => {
            if (result.value) {
                $.post('<?php echo site_url() ?>/tarea/remove_tarea', { tarea:tarea})
                .done( function (response)
                {
                    let json = JSON.parse(response);
                    if(json.resp)
                        toast({title:'Tarea Eliminada.', type:'success'});
                    else
                        toast({title:'No se logro realizar esta acción.', type:'error'});
                });
                getTareas();
            }
        });
    });

    $(document).on("click", ".btn-observaciones", function ()
    {
        tarea = $(this).attr('tarea');
        $.post( "<?php echo site_url() ?>/pruebaCruzada/getObservacion", {tarea:tarea} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            $("#observacion").html(json.observacion);
            $("#modal-observaciones").modal();
        });
    });

    $(document).on("click", ".detalle-tarea", function ()
    {
        tarea = $(this).attr("id-tarea");
        $.post( '<?php echo site_url() ?>/tarea/getDescripcion', {tarea:tarea})
        .done( function ( response ) 
        {
            let json = JSON.parse( response );

            $("#estado").html(json.estado);
            $("#prioridad").html(json.prioridad);
            $("#fecha").html(json.fecha);
            $("#nameTarea").html(json.tarea);
            $("#descripcion").html(json.descripcion);

            $("#modal-descripcion").modal();
        });
    });

    $(document).on("click", "#tareas-merged", function()
    {
        
        $("#table-merged").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 10,
            "order" : [[0, 'desc']],
            "ajax" : {
                "url" : "<?php echo site_url() ?>/tarea/getTareaMerged",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'informante'},
                {data: 'codigo'},
                {data: 'prioridad'},
                {data: 'acciones'}
            ]
        });
        $("#table-merged_filter").remove();
    });

    var getTareas = function()
    {
        $.ajax({
            url: "<?php echo site_url() ?>/tarea/getTareas",
            success: function (json)
            {
                $("#loading").modal("hide");
                
                json = JSON.parse( json );
                $("#1").html(json.tareas.asignada);
                $("#n1").html(json.tareas.nAsignadas);
                $("#2").html(json.tareas.desarrollo);
                $("#n2").html(json.tareas.nDesarrollo);
                $("#3").html(json.tareas.entregada);
                $("#n3").html(json.tareas.nEntregada);
                $("#4").html(json.tareas.testing);
                $("#n4").html(json.tareas.nTesting);
                $("#5").html(json.tareas.finalizada);
                $("#n5").html(json.tareas.nFinalizada);
                $("#6").html(json.tareas.merged);
                $("#n6").html(json.tareas.nMerged);

                $( ".column" ).sortable(
                {
                    connectWith: ".column",
                    handle: ".portlet-header",
                    cancel: ".portlet-toggle",
                    placeholder: "portlet-placeholder ui-corner-all"
                });
            
                $( ".portlet" )
                .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-plusthick portlet-toggle' close='true'></span>");
            
                $( ".portlet-toggle" ).on( "click", function() 
                {
                    var icon = $( this );
                    icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
                    icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
                });
            }
        });
    }

    $(document).on("click", ".flujo-tarea", function ()
    {
        let btn = $(this);
        let estado = btn.attr('detenida');
        let tarea = btn.attr('id-tarea');
        $.post( "<?php echo site_url() ?>/tarea/correrPausar", {estado:estado, tarea:tarea} )
        .done( function ( json )
        {
            json = JSON.parse( json );
            if ( json.resp )
            {
                if ( estado == 'f' )
                {
                    btn.find('.fe').toggleClass('fe-pause fe-play');
                    btn.toggleClass('btn-success btn-warning');
                    btn.attr('detenida', 't');
                    $("#cont-label-"+tarea).html("<span id='label-flujo-"+tarea+"' class='badge badge-warning'>DETENIDA</span>");
                }
                else
                {
                    btn.find('.fe').toggleClass('fe-pause fe-play');
                    btn.toggleClass('btn-success btn-warning');
                    btn.attr('detenida', 'f');
                    $("#label-flujo-"+tarea).remove();
                }
            }
        });
    });

</script>