
<div class="header menu-dark d-lg-flex p-0 collapse" id="header-menu" style="">
    <div class="container">
        <div class="row align-items-center">
            <div class="ml-auto mr-auto">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/developer/pizarra"; ?>" class="nav-link <?php echo $menu[ 'pizarra' ] ?>"><i class="fe fe-calendar"></i> Pizarra Tareas</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url()."/developer/testing"; ?>" class="nav-link <?php echo $menu[ 'testing' ] ?>"><i class="fe fe-activity"></i> Testing</a>
                    </li> 
                </ul>
            </div>
        </div>
    </div>
</div>