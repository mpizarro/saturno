
<div class="page-content">
    <div class="container">
        <div class="col-lg-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div style="width:100%">
                        <h3 class="card-title" style="float:left">Historial Tickets</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="tabla-historial" class="table card-table table-vcenter text-nowrap">
                        <thead>
                            <tr>
                                <th># ID</th>
                                <th>Informante</th>
                                <th>Tarea</th>
                                <th>Estado Actual</th>
                                <th>Prioridad</th>
                                <th>Historial</th>
                            </tr>
                        </thead>
                        <tbody
                        >
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>           
</div>


<!-- Modal pruebas cruzadas -->
<div class="modal fade" tabindex="-1" id="modal-historial" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fe fe-clock"></i> Historial</h4>
            </div>
            <div class="modal-body">

                <div class="time-line">
                    <div class="content-time-line" id="line_time">

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.Modal pruebas cruzadas -->

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script>
    $(document).ready( function()
    {
        getTablaTickets();
    });

    $(document).on("click", ".btn-historial", function() 
    {
        let ticket = $(this).attr('id-ticket');
        $.post( '<?php echo site_url() ?>/historial/getLineTimeTickets', {ticket:ticket})
        .done( function ( json  ) {
            json = JSON.parse( json );
            $("#line_time").html( json.line );
        });

        $("#modal-historial").modal();
    });

    let getTablaTickets = function()
    {
        $("#tabla-historial").dataTable({
            "destroy" : true,
            "processing" : true,
            "serverSide" : true,
            "lengthChange": false,
            "pageLength" : 10,
            "order" : [[0, 'desc']],
            "ajax" : {
                "url" : "<?php echo site_url() ?>/ticket/getTablaTickets",
                "type" : "post"
            },
            "columns" : [
                {data: 'id'},
                {data: 'departamento'},
                {data: 'asunto'},
                {data: 'estado'},
                {data: 'prioridad'},
                {data: 'acciones'}
            ]
        });
        $("#tabla-historial_filter").remove();
    }
</script>