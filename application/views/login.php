<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <!-- Generated: 2018-03-27 13:25:03 +0200 -->
    <title>Saturno | Login</title>
    <link rel="icon" href="<?php echo base_url() ?>assets/images/saturno.png" type="image/png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    
    <!-- Dashboard Core -->
    <link href="<?php echo base_url()."assets/css/dashboard.css"; ?>" rel="stylesheet">

    <style>
      body {
        background: url(<?php echo base_url() ?>assets/images/saturno-banner.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        overflow: hidden;
      }
    </style>

  </head>

    <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <!--<img src="./assets/brand/tabler.svg" class="h-6" alt="">-->
              </div>
              <form class="card card-login-dark" style="border-radius: 20px;" action="javascript:void(0)">
                <div class="card-body p-6">
                  <div class="card-title">Inicia Sesión</div>
                  <div class="form-group">
                    <label class="form-label">Usuario</label>
                    <input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="username" required>
                  </div>
                  <div class="form-group">
                    <label class="form-label">Contraseña</label>
                    <div class="row gutters-sm">
                      <div class="col">
                      <input type="password" class="form-control" id="pass" placeholder="Password" required>
                      </div>
                      <span id="show-pass" show="false" class="col-auto align-self-center">
                        <i class="show-pass fe fe-eye-off"></i>
                      </span>
                    </div>
                  </div>
                  <div class="form-footer">
                    <button type="submit" id="submit" class="btn btn-primary btn-block">Ingresar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- JQuery -->
    <script src="<?php echo base_url() ?>assets/js/vendors/jquery-3.2.1.min.js"></script>
    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>
    <script>
      $(function ()
      {
        $("#submit").click( function ()
        {
          if ($("#username").val() != "" && $("#pass").val() != "") 
          {
            $.ajax
            ({
              url: "<?php echo base_url() ?>index.php/login/check_login",
              type: "POST",
              data: {username:$("#username").val(), password:$("#pass").val()},
              success: function (response)
              {
                let json = JSON.parse(response);
                console.log(json.is_logged);
                if (json.is_logged)
                  window.location.href = "<?php echo site_url() ?>/login";
                else
                  swal("¡Error!", "usuario o contraseña incorrectos.", "error");
              }
            });  
          }
        });
      });

      $("#show-pass").click( function ()
      {
          $(".show-pass").toggleClass('fe-eye-off');
          $(".show-pass").toggleClass('fe-eye');

          if($("#pass").attr("type") == "password")
              $("#pass").attr("type", "text");
          else 
              $("#pass").attr("type", "password");
      });
    </script>


  </body>
</html>