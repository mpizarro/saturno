
<div class="page-content">
    <div class="container">

        <div class="row">
            <div class="col-12 card card-dark">
                <div class="card-header">
                    <h3 class="card-title title-testing">Pruebas cruzadas</h3>
                    <i class="fe fe-activity icon-testing" title="Prueba cruzada en progreso..."></i>
                </div>
                <div class="card-body pt-0 table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th colspan="2">Creada por</th>
                                <th colspan="2">Para</th>
                                <th>Tarea</th>
                                <th>Prioridad</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="testing-progress">
                            <tr>
                                <td colspan="9">
                                    <center><img src="<?php echo base_url() ?>assets/images/loading.gif" alt="LOADING" width="40px"></center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" id="modal-finalizar-prueba-cruzada" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"> Finalizar prueba cruzada</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-label">Observación</label>
                                <div class="input-icon mb-3">
                                    <textarea type="text" id="obsevacion" class="form-control" placeholder="Opcional..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cancelar</button>
                        <button type="button" class="btn btn-success btn-finalizar" estado="12">Finalizar con exíto</button>
                        <button type="button" class="btn btn-warning btn-finalizar" estado="13">Finalizar con errores</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
    </div>
</div>

<script type="text/javascript">

var id = 0;

$(document).ready( function () 
{
    getPruebasCruzadas();
});

function getPruebasCruzadas() 
{
    $.ajax({
        url: '<?php echo site_url() ?>/PruebaCruzada/getPruebaCruzada',
        success: function( json ) 
        {
            json = JSON.parse(json);
            $("#testing-progress").html( json.rows );
        }
    });
}

$(document).on("click", ".btn-change-estado", function() 
{
    let idPruebaCruzada = $(this).attr('id');
    let estado = $(this).attr('estado');
    changeEstado(estado, idPruebaCruzada);
});

$(document).on("click", ".btn-modal-finalizar", function () 
{
    id = $(this).attr('id');
    $("#modal-finalizar-prueba-cruzada").modal();
});

$(".btn-finalizar").click( function ()
{
    let estado = $(this).attr('estado');
    let obsevacion = $("#obsevacion").val();

    changeEstado(estado, id, obsevacion);
    $("#modal-finalizar-prueba-cruzada").modal('hide');
});

$(document).on("click", ".btn-descripcion", function ()
{
    let tarea = $(this).attr('id');
    $.post( '<?php echo site_url() ?>/tarea/getDescripcion', {tarea:tarea})
        .done( function ( response ) 
        {
            let json = JSON.parse( response );
            $("#descripcion").html(json.descripcion);
            swal('Descripción de la tarea', json.descripcion, 'info');
        });
});

var changeEstado = function (estado, idPruebaCruzada, observacion=false)
{
    let data = {
        estado:estado,
        idPruebaCruzada:idPruebaCruzada, 
        observacion:observacion
    };
    $.post( '<?php echo site_url() ?>/PruebaCruzada/changeEstado', data )
    .done( function ( json )
    {
        json = JSON.parse( json );
        if (json.resp > 0)
            toast({type:'success', title:'¡Perfecto!'});
        else
            toast({type:'danger', title:'¡Error!'});
        getPruebasCruzadas();
    });
}
    
</script>