<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('tarea_model');
		$this->load->model('pruebaCruzada_model');
		$this->load->model('ticket_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");
	}

	public function getNotificaciones()
	{
		$data['notificaciones']	= "";
		$idUsuario = $this->session->userdata('id');
		$departamentoId = $this->session->userdata('departamento_id');

		$url = "";
		if ($this->session->userdata('perfil_id') == 4)
			$url = site_url() . "/administrador";
		elseif ($this->session->userdata('perfil_id') == 1)
			$url = site_url() . "/developer";
		elseif ($this->session->userdata('perfil_id') == 2)
			$url = site_url() . "/departamento";
		
		$data['tareas'] = false;
		$dataWhere = array('usuario_id'=>$idUsuario, 'estado_id'=>1);
		if ($tareas = $this->tarea_model->getForNotificacion($dataWhere))
		{
			$n = 0;
			foreach ($tareas as $tarea)
			{
				$data['tareas'][] = $tarea->id;
				$n++;
			}

			$data['notificaciones'] .= ($n > 0) ? 
			"<a class='dropdown-item item-alert' href='".$url."/pizarra'>
				<span class='float-right'>
					<span class='badge badge-info'>".$n."</span>
				</span>
				<span>Tareas asignadas</span>
			</a>" : "";
		}

		$data['pruebas'] = false;
		$dataWhere = array('usuario_solicitado'=>$idUsuario, 'estado_id'=>10);
		if ($pruebas = $this->pruebaCruzada_model->getForNotificacion($dataWhere))
		{
			$n = 0;
			foreach ($pruebas as $prueba)
			{
				$data['pruebas'][] = $prueba->id;
				$n++;
			}

			$data['notificaciones'] .= ($n > 0) ? 
			"<a class='dropdown-item item-alert' href='".$url."/testing'>
				<span class='float-right'>
					<span class='badge badge-warning'>".$n."</span>
				</span>
				<span>Solicitudes de testing</span>
			</a>" : "";
		}

		$data['tickets'] = false;
		if ($this->session->userdata('perfil_id') == 2 || $this->session->userdata('perfil_id') == 4)
		{
			$dataWhere = array('departamento_destino_id'=>$departamentoId, 'estado_id'=>1);
			if ($tickets = $this->ticket_model->getForNotificacion($dataWhere))
			{
				$n = 0;
				foreach ($tickets as $ticket)
				{
					$data['tickets'][] = $ticket->id;
					$n++;
				}
				
				$data['notificaciones'] .= ($n > 0) ? 
				"<a class='dropdown-item item-alert' href='".$url."/tickets'>
					<span class='float-right'>
						<span class='badge badge-danger'>".$n."</span>
					</span>
					<span>Tickets asignados</span>
				</a>" : "";
			}
		}

		echo json_encode($data);
	}
}