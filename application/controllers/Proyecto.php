<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyecto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('proyecto_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");
	}

	public function getProyectos ()
	{
		$start = $this->input->post('start');
        $length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		
		$data = array();
		$proyectos = $this->proyecto_model->getAll(array('length'=>$length, 'start'=>$start, 'search'=>$search));
		if ( $proyectos['result'] )
		{
			foreach ( $proyectos['result'] as $proyecto )
			{
				$data[] = array(
					'id' => $proyecto->id,
					'descripcion' => $proyecto->descripcion,
					'codigo' => $proyecto->codigo,
					'fecha' => $proyecto->fecha,
					'acciones' => "<button type='button' title='Eliminar' class='btn btn-danger remove-proyecto' id-proyecto='".$proyecto->id."'><i class='fe fe-trash-2'></i></button>
						<button type='button' title='Editar' class='btn btn-info update-proyecto' id-proyecto='".$proyecto->id."'><i class='fe fe-edit'></i></button>"
				);
			}
		}

		$json_data = array(
			'draw' => intval($this->input->post('draw')),
			'recordsTotal' => intval($proyectos['rows']),
			'recordsFiltered' => intval($proyectos['total_rows']),
			'data' => $data
		);
		echo json_encode($json_data);
	}

	public function getProyecto()
	{
		$idProyecto = $this->input->post('id_proyecto');
		$data = array();
		if ($proyecto = $this->proyecto_model->getProyectoById($idProyecto))
			$data = array(
				'nombre' => $proyecto->descripcion,
				'codigo' => $proyecto->codigo
			);
		echo json_encode($data);
	}

	public function update_proyecto($idProyecto)
	{
		$dataUpdate = array(
			'descripcion' => $this->input->post('name'),
			'codigo' => $this->input->post('codigo')
		);
		$response['response'] = $this->proyecto_model->update_proyecto($idProyecto, $dataUpdate);
		echo json_encode($response);
	}

	public function save_proyecto ()
	{
		$data =array(
				'descripcion' => $this->input->post( 'name' ),
				'codigo' => $this->input->post( 'codigo' ),
				'fecha' => date( 'Y-m-d' )
		);
		$return['resp'] = $this->proyecto_model->save_proyecto( $data );

		echo json_encode( $return );
	}

	public function remove_proyecto ()
	{
		$id_proyecto = $this->input->post( 'id_proyecto' );
		$return['resp'] = $this->proyecto_model->remove_proyecto( $id_proyecto );

		echo json_encode( $return );
	}

	public function selectProyectos()
	{
		$data['select'] = '<option value="0" hidden>Seleccione...</option>';
		$proyectos 	= $this->proyecto_model->getAll(array('length'=>false));
		if($proyectos['result'])
			foreach($proyectos['result'] as $proyecto)
				$data['select'].= '<option value="'.$proyecto->id.'" >'.$proyecto->descripcion.'</option>';

		echo json_encode($data);
	}
}
