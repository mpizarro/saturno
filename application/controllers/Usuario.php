<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('usuario_model');
		$this->load->model('tarea_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");
	}

	public function getUsuarios()
	{
        $start = $this->input->post('start');
        $length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		
		$data = array();
		$usuarios = $this->usuario_model->getAll(array('start'=>$start, 'length'=>$length, 'search'=>$search));
		if ( $usuarios['result'] ) 
			foreach ($usuarios['result'] as $usuario ) 
			{
				$porcentajePromedio = 0;
				$porcentaje = 0;
				$puntaje = 0;
				$n = 1;
				if ($rankings = $this->tarea_model->getRankingByUsuario($usuario->id))
				{
					foreach ($rankings as $ranking) 
					{
						$porcentaje += $ranking->porcentaje_cumplimiento;
						$puntaje += $ranking->puntaje;
						$n++;
					}
				}
				$porcentajePromedio = round($porcentaje / $n, 1);

				$labelCategoria = '';
				$imgCategoria = '';
				if ($porcentajePromedio <= 10)
				{
					$labelCategoria = 'Youngling Jedi';
					$imgCategoria = 'mina.jpeg';
				}
				elseif ($porcentajePromedio > 10 && $porcentajePromedio <= 30)
				{
					$labelCategoria = 'Padawan';
					$imgCategoria = 'padawan.jpeg';
				}
				elseif ($porcentajePromedio > 30 && $porcentajePromedio <= 70)
				{
					$labelCategoria = 'Caballero Jedi';
					$imgCategoria = 'jedi.jpeg';
				}
				elseif ($porcentajePromedio > 70 && $porcentajePromedio <= 90)
				{
					$labelCategoria = 'Maestro Jedi';
					$imgCategoria = 'mjedi.jpeg';
				}
				elseif ($porcentajePromedio > 90)
				{
					$labelCategoria = 'Gran Maestro Jedi';
					$imgCategoria = 'kratos.png';
				}

				$data[] = array(
					'id' => $usuario->id,
					'usuario' => "<div style='width:100%;'><span class='avatar' style='float:left; margin-right:10px; background-image: url(".base_url()."/assets/images/users/".$usuario->imagen.")'></span><span style='float:left;'>" . $usuario->usuario . "</span></div>",
					'usuario2' => $usuario->nombre,
					'perfil' => $usuario->perfil,
					'categoriaAvatar' => "<div style='width:100%;'><span class='avatar' style='float:left; margin-right:10px; background-image: url(".base_url()."/assets/images/level/".$imgCategoria.")'></span><span style='float:left;'>" . $labelCategoria . "</span></div>",
					'porcentaje' => $porcentajePromedio . "%",
					'puntaje' => $puntaje,
					'acciones' => "<button type='button' title='Eliminar' class='btn btn-danger btn-modal-delet-user' id-usuario='".$usuario->id."'><i class='fe fe-trash-2'></i></button>
						<button type='button' title='Editar' class='btn btn-info btn-modal-update-user' id-usuario='".$usuario->id."'><i class='fe fe-edit'></i></button>"
				);
			}

		$json_data = array(
			'draw' => intval($this->input->post('draw')),
			'recordsTotal' => intval($usuarios['rows']),
			'recordsFiltered' => intval($usuarios['total_rows']),
			'data' => $data
		);
		echo json_encode( $json_data );
	}

	public function deleteUser()
	{
		$user = $this->input->post('user');
		$data['resp'] = $this->usuario_model->deleteUser($user);

		echo json_encode( $data );
	}

	public function updateUser( $user )
	{
		$data_update = array(
			'nombre' => $this->input->post('nombre'),
			'usuario' => $this->input->post('usuario'),
			'perfil_id' => $this->input->post('perfil'),
			'is_testing' => (($this->input->post('isTesting') == "t") ? true : false),
			'departamento_id' => $this->input->post('departamento'),
		);

		$data['resp'] = $this->usuario_model->updateUser( $user, $data_update );
		echo json_encode( $data );
	}

	public function getUsuario()
	{
		$data['nombre'] 		= '';
		$data['usuario'] 		= '';
		$data['perfil'] 		= '';
		$data['testing'] 		= '';
		$data['departamento_id'] 	= '';
		$user = $this->input->post('user');
		$usuario = $this->usuario_model->getUsuario( $user );
		if($usuario )
			$data['nombre'] 		= $usuario->nombre;
			$data['usuario'] 		= $usuario->usuario;
			$data['perfil'] 		= $usuario->perfil_id;
			$data['testing'] 		= $usuario->is_testing;
			$data['departamento_id'] 	= $usuario->departamento_id;

		echo json_encode( $data );
	}

	public function saveUser()
	{
		$data_insert = array(
			'nombre' => $this->input->post('nombre'),
			'usuario' => $this->input->post('usuario'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT, [ 'cost'=>14 ]),
			'perfil_id' => $this->input->post('perfil'),
			'departamento_id' => $this->input->post('departamento'),
			'imagen' => 'pony.jpg',
			'fecha' => date('Y-m-d'),
			'is_testing' => (($this->input->post('isTesting') == "t") ? true : false)
		);
		$data['resp'] = $this->usuario_model->saveUser( $data_insert );

		echo json_encode( $data );
	}

	public function selectUsuarios()
	{
		$data['select'] = "<option value='0' hidden>Seleccione...</option>";
		$usuarios = $this->usuario_model->getAll(array('length'=>false));
		if($usuarios['result'])
			foreach($usuarios['result'] as $usuario)
				$data['select'].= '<option value="'.$usuario->id.'" data-data="{\'image\': "'.base_url().'demo/faces/female/16.jpg"}">'.$usuario->nombre.'</option>';
		echo json_encode($data);
	}

	public function selectPerfil()
	{
		$data['select'] = "<option value='0' hidden>Seleccione...</option>";
		$perfiles 	= $this->usuario_model->getPerfiles();
		if($perfiles)
			foreach($perfiles as $perfil)
				$data['select'].= '<option value="'.$perfil->id.'">'.$perfil->perfil.'</option>';
		echo json_encode($data);
	}

	public function selectDepartamento()
	{
		$data['select'] = "<option value='0' hidden>Seleccione...</option>";
		$departamentos 	= $this->usuario_model->getDepartamento();
		if($departamentos)
			foreach($departamentos as $departamento)
				$data['select'].= '<option value="'.$departamento->id.'">'.$departamento->nombre.'</option>';
		echo json_encode($data);
	}

	public function getArrayUsers()
	{
		$idUsuario = $this->session->userdata('id');
		$data['usuarios'] = array();
		$usuarios = $this->usuario_model->getUserForTesting($idUsuario);
		if($usuarios) {
			foreach ($usuarios as $usuario) {
				$data['usuarios']['nombre'][] = $usuario->nombre;
				$data['usuarios']['id'][] = $usuario->id;
				$data['usuarios']['imagen'][] = $usuario->imagen;
			}
		} else {
			$data['usuarios'] = false;
		}
		echo json_encode($data);
	}

	public function saveAvatar()
	{
		$img = $this->input->post('img');
		$usuario = $this->session->userdata('id');
		$response['response'] = $this->usuario_model->saveAvatar($usuario, $img);
		echo json_encode( $response );
	}

	public function changePass()
	{
		$user = $this->session->userdata('id');
		$passActual = $this->input->post('passActual');
		$nuevaPass = $this->input->post('nuevaPass');

		$response['passActual'] = $this->usuario_model->checkPass($user, $passActual);
		if ( $response['passActual'] ) {
			$data = array( 'password' => password_hash($nuevaPass, PASSWORD_DEFAULT, [ 'cost'=>14 ]) );
			$response['save'] = $this->usuario_model->updateUser($user, $data);
		}

		echo json_encode($response);
	}
}
