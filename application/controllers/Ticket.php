<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('ticket_model');
		$this->load->model('departamento_model');
		$this->load->model('historial_model');
		
		if($this->session->userdata("username") == null)
			redirect("login/log_out");

		date_default_timezone_set('America/Santiago');
	}

	public function getTickets()
	{
		$data['rows'] = "";
		$departamentoId = $this->session->userdata('departamento_id');
		if ($tickets = $this->ticket_model->getBydepartamento( $departamentoId ))
			foreach ($tickets as $ticket) 
			{
				$prioridad = "";
				switch ($ticket->prioridad_id) 
				{
					case 1: $prioridad = "<span class='badge badge-success border'>".$ticket->prioridad."</span>"; break;
					case 2: $prioridad = "<span class='badge badge-warning border'>".$ticket->prioridad."</span>"; break;
					case 3: $prioridad = "<span class='badge badge-danger border'>".$ticket->prioridad."</span>"; break;
				}

				$departamentoDestinoNombre = ""; 
				$departamentoDestinoId = "";
				if ($departamentoDestino = $this->departamento_model->getById($ticket->departamento_destino_id))
					$departamentoDestinoNombre = $departamentoDestino->nombre;
					$departamentoDestinoId = $departamentoDestino->id;

				$colorEstado = "";
				$textEstado = "";
				$buttons = "<button type='button' title='Ver' class='btn btn-info btn-ver mr-1' id-ticket='".$ticket->id."'><i class='fe fe-eye'></i></button>";
				switch ($ticket->estado_id) 
				{
					case 1:
						$textEstado = "alert-link";
						if ($departamentoId == $ticket->departamento_destino_id)
							$buttons .= "<button type='button' title='Rechazar' class='btn btn-danger mr-1 btn-modal-rechazar' estado='15' id-ticket='".$ticket->id."'><i class='fe fe-alert-triangle'></i></button>
										<button type='button' title='Recibir' class='btn btn-info mr-1 btn-change-estado' estado='2' id-ticket='".$ticket->id."'><i class='fe fe-arrow-down-left'></i></button>";
						elseif ($departamentoId == $ticket->departamento_creador_id)
							$buttons .= "<button type='button' title='Editar' class='btn btn-info btn-modal-update' id-ticket='".$ticket->id."'><i class='fe fe-edit'></i></button>
										<button type='button' title='Eliminar' class='btn btn-gray mr-1 btn-change-estado' estado='7' id-ticket='".$ticket->id."'><i class='fe fe-x'></i></button>";
						break;
					case 2:
						$colorEstado = "bg-white";
						$textEstado = "alert-link text-default";
						if ($departamentoId == $ticket->departamento_destino_id)
							$buttons .= "<button type='button' title='Entregar' class='btn btn-warning mr-1 btn-change-estado' estado='3' id-ticket='".$ticket->id."'><i class='fe fe-arrow-up-right'></i></button>";
						break;
					case 3: 
						$colorEstado = "bg-yellow"; 
						$textEstado = "text-white";
						if ($departamentoId == $ticket->departamento_creador_id)
							$buttons .= "<button type='button' title='Rechazar' class='btn btn-danger mr-1 btn-modal-rechazar' estado='15' id-ticket='".$ticket->id."'><i class='fe fe-alert-triangle'></i></button>
							<button type='button' title='Completar' class='btn btn-success mr-1 btn-change-estado' estado='14' id-ticket='".$ticket->id."'><i class='fe fe-check'></i></button>";
						break;
					case 14: 
						$colorEstado = "bg-green"; 
						$textEstado = "text-white";
						if ($departamentoId == $ticket->departamento_creador_id)
							$buttons .= "<button type='button' title='Eliminar' class='btn btn-gray mr-1 btn-change-estado' estado='7' id-ticket='".$ticket->id."'><i class='fe fe-x'></i></button>";
						break;
					case 15: 
						$colorEstado = "bg-red";
						$textEstado = "text-white";
						$buttons = "<button type='button' title='Ver motivo' class='btn btn-danger mr-1 btn-ver-rechazo mr-1 border' id-ticket='".$ticket->id."'><i class='fe fe-eye'></i></button>";
						if ($departamentoId == $ticket->departamento_creador_id)
							$buttons .= "<button type='button' title='Eliminar' class='btn btn-gray border btn-change-estado' estado='7' id-ticket='".$ticket->id."'><i class='fe fe-x'></i></button>
										<button type='button' title='Reasignar' class='btn btn-primary mr-1 btn-modal-reasignar' estado='16' id-ticket='".$ticket->id."'><i class='fe fe-rotate-cw'></i></button>";
						break;
					case 16:
						$colorEstado = "bg-red";
						$textEstado = "alert-link text-white";
						if ($departamentoId == $ticket->departamento_destino_id)
							$buttons .= "<button type='button' title='Rechazar' class='btn border btn-danger mr-1 btn-modal-rechazar' estado='15' id-ticket='".$ticket->id."'><i class='fe fe-alert-triangle'></i></button>
										<button type='button' title='Recibir' class='btn btn-info mr-1 btn-change-estado' estado='2' id-ticket='".$ticket->id."'><i class='fe fe-arrow-down-left'></i></button>";
						elseif ($departamentoId == $ticket->departamento_creador_id)
							$buttons .= "<button type='button' title='Editar' class='btn btn-info btn-modal-update' id-ticket='".$ticket->id."'><i class='fe fe-edit'></i></button>
										<button type='button' title='Eliminar' class='btn btn-gray mr-1 btn-change-estado' estado='7' id-ticket='".$ticket->id."'><i class='fe fe-x'></i></button>";
						break;
				}

				$data['rows'] .= "<tr class='".$colorEstado." ".$textEstado."'>
					<td>".$ticket->id."</td>
					<td>".$ticket->asunto."</td>
					<td>".$prioridad."</td>
					<td>".strtoupper($ticket->departamento)."</td>
					<td>".strtoupper($departamentoDestinoNombre)."</td>
					<td>".$ticket->fecha."</td>
					<td>".$ticket->estado."</td>
					<td class='cont-btn'>".$buttons."</td>
				</tr>";
			}

		echo json_encode($data);
	}

	public function nuevo()
	{
		$data = array(
			'detalle' => $this->input->post('requerimiento'),
			'estado_id' => 1,
			'departamento_creador_id' => $this->session->userdata('departamento_id'),
			'departamento_destino_id' => $this->input->post('destino'),
			'prioridad_id' => $this->input->post('prioridad'),
			'fecha' => date("Y-m-d H:i:s"),
			'asunto' => $this->input->post('asunto')
		);
		$response['response'] = $this->ticket_model->save($data);
		echo json_encode($response);
	}

	public function getTicket()
	{
		$data = array();
		$id = $this->input->post('id');
		if ($ticket = $this->ticket_model->getTicket( $id ))
			$data = array(
				'asunto' => $ticket->asunto,
				'prioridad' => $ticket->prioridad_id,
				'destino' => $ticket->departamento_destino_id,
				'requerimiento' => $ticket->detalle,
				'motivoRechazo' => $ticket->motivo_rechazo
			);

		echo json_encode($data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$dataUpdate = array(
			'asunto'					=> $this->input->post('asunto'),
			'prioridad_id'				=> $this->input->post('prioridad'),
			'departamento_destino_id'	=> $this->input->post('destino'),
			'detalle'					=> $this->input->post('requerimiento')
		);
		$response['response'] = $this->ticket_model->update( $id, $dataUpdate );
		echo json_encode($response);
	}

	public function changeEstado()
	{
		$id = $this->input->post('id');
		$estadoId = $this->input->post('estado');
		$dataUpdate = array(
			'estado_id'	=> $estadoId,
			'motivo_rechazo' => $this->input->post('motivo')
		);
		if ( $this->input->post('requerimiento') )
			$dataUpdate['detalle'] = $this->input->post('requerimiento');

		$response['response'] = $this->ticket_model->update( $id, $dataUpdate );

		$historial = array(
			"historial" 	=>  estadoByIdForHistorialTicket($estadoId),
			"ticket_id" 		=> $id,
			"estado_historico" => $estadoId,
			"fecha" 		=> date("Y-m-d H:i:s"),
			'tipo' 			=> 2
		);
		$this->historial_model->save($historial);

		echo json_encode($response);
	}

	public function selectPrioridad()
	{
		$data['select'] = "<option value='null' hidden>Seleccione...</option>";
		$prioridades 	= $this->ticket_model->getPrioridad();
		if($prioridades)
			foreach($prioridades as $prioridad)
				$data['select'].= '<option value="'.$prioridad->id.'">'.$prioridad->prioridad.'</option>';
		echo json_encode($data);
	}

	public function selectDepartamento()
	{
		$data['select'] = "<option value='null' hidden>Seleccione...</option>";
		$departamentos 	= $this->departamento_model->getAll( $this->session->userdata('departamento_id') );
		if($departamentos)
			foreach($departamentos as $departamento)
				$data['select'].= '<option value="'.$departamento->id.'">'.$departamento->nombre.'</option>';
		echo json_encode($data);
	}

	public function getTablaTickets()
	{
		$start = $this->input->post('start');
        $length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		
		$data = array();
		$departamentoId = $this->session->userdata('departamento_id');
		$tickets = $this->ticket_model->getByDepartamentoHistorial($departamentoId, $start, $length, $search);
		if ($tickets['result'])
		{
			foreach ($tickets['result'] as $ticket) 
			{
				$prioridad = "";
				switch ($ticket->prioridad_id) 
				{
					case 1: $prioridad = "<span class='badge badge-success border'>".$ticket->prioridad."</span>"; break;
					case 2: $prioridad = "<span class='badge badge-warning border'>".$ticket->prioridad."</span>"; break;
					case 3: $prioridad = "<span class='badge badge-danger border'>".$ticket->prioridad."</span>"; break;
				}

				$estado = "";
				switch ($ticket->estado_id) 
				{
					case 1: $estado = "<span class='badge badge-info border'>".$ticket->estado."</span>"; break;
					case 2: $estado = "<span class='badge bg-primarry border'>".$ticket->estado."</span>"; break;
					case 3: $estado = "<span class='badge badge-warning border'>".$ticket->estado."</span>"; break;
					case 14: $estado = "<span class='badge badge-success border'>".$ticket->estado."</span>"; break;
					case 15: $estado = "<span class='badge badge-danger border'>".$ticket->estado."</span>"; break;
					case 16: $estado = "<span class='badge badge-danger border'>".$ticket->estado."</span>"; break;
					case 7: $estado = "<span class='badge badge-dark border'>".$ticket->estado."</span>"; break;
				}

				$data[] = array(
					'id' => $ticket->id,
					'departamento' => strtoupper($ticket->departamento),
					'asunto' => $ticket->asunto,
					'estado' => $estado,
					'prioridad' => $prioridad,
					'acciones' => "<a id-ticket='".$ticket->id."' title='Ver historia de la tarea' href='javascript:void(0)' class='btn-historial'><i class='fe fe-clock'></i></a>"
				);
			}
		}
		$json_data = array(
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($tickets['rows']),
            'recordsFiltered' => intval($tickets['total_rows']),
            'data' => $data
        );

		echo json_encode($json_data);
	}
}
