<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backlog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('backlog_model');
		$this->load->model('tarea_model');
		$this->load->model('historial_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");

		date_default_timezone_set('America/Santiago');
	}

	public function getBacklogs()
	{
		$start = $this->input->post('start');
        $length = $this->input->post('length');
		$search = $this->input->post('search')['value'];

		$data = array();
		$backlogs = $this->backlog_model->getAll(array('start'=>$start, 'length'=>$length, 'search'=>$search));
		if($backlogs['result'])
			foreach($backlogs['result'] as $backlog)
			{
				$data[] = array(
					'id' => $backlog->id,
					'asunto' => $backlog->asunto,
					'descripcion' => $backlog->descripcion,
					'codigo' => $backlog->codigo,
					'informador' => "<div style='width:100%;'><span class='avatar' style='float:left; margin-right:10px; background-image: url(".base_url()."/assets/images/users/".$backlog->imagen.")'></span><span style='float:left;'>".$backlog->nombre."</span></div>",
					'fecha' => formatDate($backlog->fecha),
					'acciones' => '<button title="Asignar" backlog_id="'.$backlog->id.'" type="button" class="btn btn-info btnModalAsignarBacklog"><i class="fe fe-check-square"></i>
						<button title="Editar" backlog_id="'.$backlog->id.'" type="button" class="btn btn-info p-0 ml-1 btnModalUpdateBacklog"><i class="fe fe-edit"></i></button>
						<button title="Descripción" backlog_id="'.$backlog->id.'" type="button" class="btn btn-info btnDetalleBacklog"><i class="fe fe-list"></i></button>
						<button title="Eliminar" backlog_id="'.$backlog->id.'" type="button" class="btn btn-danger btnEliminarBacklog"><i class="fe fe-trash-2"></i></button>'
				);
			}
		$json_data = array(
			'draw' => intval($this->input->post('draw')),
			'recordsTotal' => intval($backlogs['rows']),
			'recordsFiltered' => intval($backlogs['total_rows']),
			'data' => $data
		);
		echo json_encode($json_data);
	}

	public function selectDificultad()
	{
		$data['select'] = '<option value="0" hidden>Seleccione...</option>';
		$dificultad = $this->tarea_model->getDificultad();
		if($dificultad)
			foreach ($dificultad as $dif)
				$data['select'].= '<option value="'.$dif->id.'" >'.$dif->dificultad.'</option>';
		echo json_encode($data);
	}

	public function save_backlog()
	{
		$proyecto 				= $this->input->post('proyecto');
		$codigo 				= $this->backlog_model->getCodigo($proyecto);

		$backlog['codigo'] 		= $codigo['codigo']."-".($codigo['sn']+1);
		$backlog['asunto']		= $this->input->post('asunto');
		$backlog['backlog'] 	= $this->input->post('descripcion');
		$backlog['usuario_id']	= $this->input->post('usuario_id');
		$backlog['fecha']		= date("Y-m-d H:i:s");
		$backlog['proyecto_id']	= $proyecto;
		$backlog['sn'] 			= $codigo['sn']+1;

		$backlogId = $this->backlog_model->save($backlog);
		$data['response_file'] = "error";
		if($backlogId) {
			// if (count($_FILES) > 0) {
			// 	$insertFileName = array();
			// 	for ($i = 0; $i < count($_FILES); $i++) {
			// 		$ruta = "/var/www/html/saturno/saturno/files/" . $_FILES[$i]['name'];
			// 		$link = $_FILES[$i]['name'];
			// 		$insertFileName[] = array(
			// 			'ruta' => $link,
			// 			'backlog_id' => $backlogId,
			// 			'extension' => pathinfo($_FILES[$i]['name'], PATHINFO_EXTENSION)
			// 		);
			// 		move_uploaded_file($_FILES[$i]['tmp_name'], $ruta);
			// 	}
			// 	$data['response_file'] = $this->backlog_model->saveAdjunto($insertFileName);
			// }
			$data['response'] = "success";
		} else {
			$data['response'] = "error";
		}
		echo json_encode($data);
	}

	public function downloadFile()
	{
		$nameFile = $this->input->get('nameFile');
		$ruta = "saturno/files/";

	 	header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($ruta.$nameFile));
		header("Content-Disposition: attachment; filename=$nameFile");
		flush();
		readfile($ruta.$nameFile);
	}
	
	public function getDetalle()
	{
		$data['detalle'] = "";
		$backlog_id = $this->input->post('backlog');
		$backlog = $this->backlog_model->get( $backlog_id );
		if ( $backlog ) {
			$data['detalle'] = $backlog->backlog;
			$data['asunto'] = $backlog->asunto;
			$data['codigo'] = $backlog->codigo;
			$data['proyecto'] = $backlog->proyecto;
		}
		echo json_encode( $data );
	}

	public function removeBacklog()
	{
		$backlog_id = $this->input->post('backlog');
		$data['resp'] = $this->backlog_model->removeBacklog($backlog_id);
		echo json_encode($data);
	}

	public function asignatarea()
	{
		$usuario    = $this->input->post('usuario');
		$backlog    = $this->input->post('backlog');
		$prioridad  = $this->input->post('prioridad');
		$dificultad = $this->input->post('dificultad');
		$horas		= $this->input->post('horas');

		$backlogRow = $this->backlog_model->get($backlog);

		$tarea['usuario_id'] 	= $usuario;
		$tarea['estado_id']		= 1;
		$tarea['backlog_id']	= $backlog;
		$tarea['prioridad_id']	= $prioridad;
		$tarea['dificultad_id'] = $dificultad;
		$tarea['horas_estimadas'] = $horas;
		$tarea['estimacion']	= 0;
		
		$Backlog['estado_id']   = 1;

		$resp = $this->tarea_model->save($tarea);

		$dataUpdate = array('estado_id'=>7);
		$this->backlog_model->update($dataUpdate, $backlog);

		if ( $resp['resp'] ) {
			$data['success'] = "success";
			$historial = array(
				"historial" 	=> 'Tarea asignada',
				"tarea_id" 		=> $resp['tarea_id'],
				"estado_historico" => 1,
				"fecha" 		=> date("Y-m-d H:i:s"),
				"tipo" 			=> 1
			);
			$this->historial_model->save($historial);
		} else {
			$data['success'] = "error";
		}
		echo json_encode($data);
	}

	public function getBacklog()
	{
		$id = $this->input->post('id');
		$data = array();
		if ($backlog = $this->backlog_model->get( $id ))
			$data = array(
				'asunto' => $backlog->asunto,
				'proyecto' => $backlog->proyecto_id,
				'descripcion' => $backlog->backlog
				//'adjuntos' => $this->getAdjuntos($id)
			);
		echo json_encode( $data );
	}

	public function getAdjuntos($id)
	{
		$adjuntosHtml = "";
		if ($adjuntos = $this->backlog_model->getAdjuntosByBacklog($id))
			foreach ($adjuntos as $adjunto) {
				$img = "format.png";
				switch ($adjunto->extension) {
					case 'png': $img = "png.png"; break;
					case 'jpg': $img = "jpg.png"; break;
					case 'jpeg': $img = "jpeg.png"; break;
				}
				$adjuntosHtml .= "<div class='content-adjunto col-2' cont-adjunto='cont-adj-$adjunto->id'>
					<img src='".base_url()."assets/images/files/$img' alt='Adjunto' download-file file='$adjunto->ruta' width='75%'>
					<i class='fe fe-x-circle btn-delete-adjunto' id-adjunto='$adjunto->id' btn-delete-adjunto></i>
				</div>";
			}
		return $adjuntosHtml;
	}

	public function eliminarAdjunto()
	{
		$backlogId = $this->input->post('id');
		$response['response'] = $this->backlog_model->eliminarAdjuntoById($backlogId);
		echo json_encode($response);
	} 

	public function update()
	{
		$backlogId = $this->input->post('id');
		$dataUpdate = array(
			'backlog' => $this->input->post('descripcion'),
			'asunto' => $this->input->post('asunto'),
			'proyecto_id' => $this->input->post('proyecto')
		);

		$response['response'] = $this->backlog_model->update($dataUpdate, $backlogId);
		
		/*if (count($_FILES) > 0) {
			$insertFileName = array();
			for ($i = 0; $i < count($_FILES); $i++) {
				$ruta = "/var/www/html/saturno/saturno/files/" . $_FILES[$i]['name'];
				$link = $_FILES[$i]['name'];
				$insertFileName[] = array(
					'ruta' => $link,
					'backlog_id' => $backlogId,
					'extension' => pathinfo($_FILES[$i]['name'], PATHINFO_EXTENSION)
				);
				move_uploaded_file($_FILES[$i]['tmp_name'], $ruta);
			}
			$data['response_file'] = $this->backlog_model->saveAdjunto($insertFileName);
		}*/
		echo json_encode($response);
	}
}
