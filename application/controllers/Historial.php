<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');
		
		$this->load->model('historial_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");
		
		date_default_timezone_set('America/Santiago');
	}

	function getLineTimeTareas()
	{
		$idTarea = $this->input->post('tarea');
		$data['line'] = "";
		$historial = $this->historial_model->getHistorial($idTarea, 1);
		if($historial) {
			foreach ($historial as $value) {
				$fecha = date('d-m-Y', strtotime($value->fecha));
				$hora = date('H:i:s', strtotime($value->fecha));
				$letraEstado = "";
				switch ($value->estado_historico) {
					case 1: $letraEstado = "AS"; break;
					case 2: $letraEstado = "DE"; break;
					case 3: $letraEstado = "EN"; break;
					case 4: $letraEstado = "TE"; break;
					case 5: $letraEstado = "FI"; break;
					case 6: $letraEstado = "ME"; break;
					case 7: $letraEstado = "EL"; break;
					case 8: $letraEstado = "PR"; break;
					case 9: $letraEstado = "AC"; break;
					case 10: $letraEstado = "ST"; break;
					case 11: $letraEstado = "TO"; break;
				}
				$data['line'] .= "<div>
					<div class='head-time-line'>
						<label>".$fecha."</label>
						<label>".$hora."</label>
					</div>
					<span class='avatar circle-line-time ".colorEstadosHitorial($value->estado_historico)."'>".$letraEstado."</span>
					<div class='body-time-line'>
						<div>
							<p class='estado-time-line'>".$value->historial."</p>
							<p class='informante-time-line'>Por: ".$value->nombre."</p>
						</div>
					</div>
				</div>";
			}
		}

		echo json_encode($data);
	}

	function getLineTimeTickets()
	{
		$idTicket = $this->input->post('ticket');
		$data['line'] = "";
		$historial = $this->historial_model->getHistorialTicket($idTicket, 2);
		if($historial) {
			foreach ($historial as $value) {
				$fecha = date('d-m-Y', strtotime($value->fecha));
				$hora = date('H:i:s', strtotime($value->fecha));
				$letraEstado = "";
				switch ($value->estado_historico) 
				{
					case 1: 
						$letraEstado = "AS"; 
						break;
					case 2: 
						$letraEstado = "DE"; 
						break;
					case 3: 
						$letraEstado = "EN"; 
						break;
					case 14: 
						$letraEstado = "CO"; 
						break;
					case 15: 
						$letraEstado = "RE"; 
						break;
					case 16: 
						$letraEstado = "RA"; 
						break;
					case 7: 
						$letraEstado = "EL"; 
						break;
				}

				$data['line'] .= "<div>
					<div class='head-time-line'>
						<label>".$fecha."</label>
						<label>".$hora."</label>
					</div>
					<span class='avatar circle-line-time ".colorEstadosHitorialTickets($value->estado_historico)."'>".$letraEstado."</span>
					<div class='body-time-line'>
						<div>
							<p class='estado-time-line'>".$value->historial."</p>
							<p class='informante-time-line'>Por: ".$value->nombre."</p>
						</div>
					</div>
				</div>";
			}
		}
		echo json_encode($data);
	}

}
