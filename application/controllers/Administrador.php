<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	private $img = null;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('utilidades');
		$this->load->library('session');
		$this->load->model('backlog_model');
		$this->load->model('tarea_model');
		$this->load->model('usuario_model');
		$this->load->model('proyecto_model');
		$this->load->model('pruebaCruzada_model');
		$this->load->model('usuario_model');

		if($this->session->userdata("username") == null || $this->session->userdata("perfil_id") != 4)
			redirect("login/log_out");

		$imagen = $this->usuario_model->getImg( $this->session->userdata("id") )->imagen;
		$this->img = ( $imagen != null ) ? $imagen : "pony.jpg"; 
	}

	public function pizarra ()
	{
		$data['menu']	 	 = get_menu( "pizarra" );
		$data['id_usuario']  = $this->session->userdata('id');
		$data['name_user']	 = $this->session->userdata('name');
		$data['perfil_label']= $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view('header', $data);
		$this->load->view('admin/menu');
		$this->load->view('pizarra');
		$this->load->view('footer');
	}

	public function testing ()
	{
		$data['menu']	 	 = get_menu( "testing" );
		$data['id_usuario']  = $this->session->userdata('id');
		$data['name_user']	 = $this->session->userdata('name');
		$data['perfil_label']= $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view('header', $data);
		$this->load->view('admin/menu');
		$this->load->view('testing');
		$this->load->view('footer');
	}

	public function proyectos ()
	{
		$data['menu'] = get_menu( "proyectos" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'admin/proyectos');
		$this->load->view( 'footer');
	}

	public function backlogs ()
	{
		$data['menu'] = get_menu( "backlogs" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['id_usuario'] = $this->session->userdata('id');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'admin/backlog');
		$this->load->view( 'footer');
	}

	public function historial_tareas()
	{
		$data['menu'] = get_menu( "historial_tareas" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'historial-tareas');
		$this->load->view( 'footer');
	}

	public function historial_tickets()
	{
		$data['menu'] = get_menu( "historial_tickets" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'historial-tickets');
		$this->load->view( 'footer');
	}

	public function usuarios()
	{
		$data['menu'] = get_menu( "usuarios" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'admin/usuarios');
		$this->load->view( 'footer');
	}

	public function tickets()
	{
		$data['menu'] = get_menu( "tickets" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'admin/menu');
		$this->load->view( 'tickets');
		$this->load->view( 'footer');
	}
}
