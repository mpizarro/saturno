<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarea extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$this->load->helper('url');
		$this->load->helper('utilidades');

		$this->load->model('tarea_model');
		$this->load->model('historial_model');
		$this->load->model('backlog_model');
		$this->load->model('pruebaCruzada_model');
		
		if($this->session->userdata("username") == null)
			redirect("login/log_out");

		date_default_timezone_set('America/Santiago');
	}

	public function getTareas()
	{
		$data['tareas']		 = array(
			"asignada"		=> "",
			"nAsignadas"	=> 0,
			"desarrollo"	=> "", 
			"nDesarrollo"	=> 0,
			"entregada"		=> "", 
			"nEntregada"	=> 0,
			"testing"		=> "", 
			"nTesting"		=> 0,
			"finalizada"	=> "", 
			"nFinalizada"	=> 0,
			"merged"		=> "",
			"nMerged"		=> 0
		);
		$idPerfil			 = $this->session->userdata('perfil_id');
		$idUsuario			 = $this->session->userdata('id');

		if ($idPerfil == 4)
		{
			$dataWhere = array('tarea.estado_id !='=>7, 'tarea.estado_id !='=>8, 'tarea.estado_id !='=>6);
			$dataWhereMerged = array('tarea.estado_id'=>6);
		}
		else
		{
			$dataWhere = array('tarea.estado_id !='=>7, 'tarea.estado_id !='=>8, 'tarea.estado_id !='=>6, 'tarea.usuario_id'=>$idUsuario);
			$dataWhereMerged = array('tarea.estado_id'=>6, 'tarea.usuario_id'=>$idUsuario);
		}

		$tareas = $this->tarea_model->getAll($dataWhere, array('length' => false, 'limit' => false));
		$tareasMerged = $this->tarea_model->getAll($dataWhereMerged, array('length' => false, 'limit' => 5));
		$puntosObtenidos = 0;

		if ( $tareas['result'] ) 
		{
			foreach ($tareas['result'] as $tarea) 
			{
				switch ($tarea->estado_id) 
				{
					case 1: 
						$data['tareas']['asignada'] .= build_portlet($tarea, $idPerfil, $puntosObtenidos); 
						$data['tareas']['nAsignadas']++;
						break;
					case 2: 
						$data['tareas']['desarrollo'] .= build_portlet($tarea, $idPerfil, $puntosObtenidos); 
						$data['tareas']['nDesarrollo']++;
						break;
					case 3: 
						if ( $ranking = $this->tarea_model->getRanking($tarea->id) )
							$puntosObtenidos = $ranking->puntaje;
						$data['tareas']['entregada'] .= build_portlet($tarea, $idPerfil, $puntosObtenidos); 
						$data['tareas']['nEntregada']++;
						break;
					case 4: 
						if ( $ranking = $this->tarea_model->getRanking($tarea->id) )
							$puntosObtenidos = $ranking->puntaje;
						$prueba = $this->pruebaCruzada_model->getByTarea( $tarea->id );
						$data['tareas']['testing'] .= build_portlet($tarea, $idPerfil, $puntosObtenidos, $prueba);
						$data['tareas']['nTesting']++;
						break;
					case 5: 
						if ( $ranking = $this->tarea_model->getRanking($tarea->id) )
							$puntosObtenidos = $ranking->puntaje;
						$data['tareas']['finalizada'] .= build_portlet($tarea, $idPerfil, $puntosObtenidos); 
						$data['tareas']['nFinalizada']++;
						break;
				}
			}
		}

		if ( $tareasMerged['result'] ) 
		{
			foreach ($tareasMerged['result'] as $tareaM) {
				if ( $ranking = $this->tarea_model->getRanking($tareaM->id) )
					$puntosObtenidos = $ranking->puntaje;
				$data['tareas']['merged'] .= build_portlet($tareaM, $idPerfil, $puntosObtenidos);
				$data['tareas']['nMerged']++;
			}
		}

		echo json_encode($data);
	}

	public function remove_tarea ()
	{
		$tarea = $this->input->post("tarea");

		$data['resp'] = $this->tarea_model->remove($tarea);

		if ($backlog = $this->backlog_model->getByTarea($tarea)) 
		{
			$dataUpdate = array('estado_id'=>9);
			$data['respBacklog'] = $this->backlog_model->update($dataUpdate, $backlog->id);
		}
		
		$historial = array(
			"historial" 	=> 'Tarea eliminada',
			"tarea_id" 		=> $tarea,
			"estado_historico" => 7,
			"fecha" 		=> date("Y-m-d H:i:s"),
			'tipo' 			=> 1
		);
		$this->historial_model->save($historial);

		echo json_encode($data);
	}

	public function getDescripcion ()
	{
		$data['descripcion'] = "";
		$id_tarea = $this->input->post('tarea');

		$tarea = $this->tarea_model->getTarea($id_tarea);
		if ($tarea)
		{
			$prioridad = "";
			if ($tarea->prioridad_id == 1)
				$prioridad = "<span class='status-icon bg-success'></span> ".$tarea->prioridad;
			if ($tarea->prioridad_id == 2)
				$prioridad = "<span class='status-icon bg-warning'></span> ".$tarea->prioridad;
			if ($tarea->prioridad_id == 3)
				$prioridad = "<span class='status-icon bg-danger'></span> ".$tarea->prioridad;

			$colorClassE = "";
			switch ($tarea->estado_id) {
				case 1: $colorClassE = "bg-blue"; break;
				case 2: $colorClassE = "bg-orange"; break;
				case 3: $colorClassE = "bg-green"; break;
				case 4: $colorClassE = "bg-yellow"; break;
				case 5: $colorClassE = "bg-teal"; break;
				case 6: $colorClassE = "bg-purple"; break;
				case 7: $colorClassE = "bg-red"; break;
			}
			$data['estado'] = "<span class='badge ".$colorClassE."'>".$tarea->estado."</span>";
			$data['prioridad'] = $prioridad;
			$data['descripcion'] = $tarea->backlog;
			$data['tarea'] = $tarea->codigo;
			$data['fecha'] = date("d-m-Y H:i", strtotime($tarea->fecha));
		}
			
		echo json_encode($data);
	}

	public function change_estado ()
	{
		$estado = $this->input->post('estado');
		$fecha = false;
		if ( $estado == 2 )
			$fecha = date("Y-m-d H:i");

		$tareaId = $this->input->post('tarea');
		$usuario = $this->input->post('usuario');

		if ($estado > 2 && $tarea = $this->tarea_model->getTarea( $tareaId ))
		{
			$calRanking = calcularRanking($tarea->horas_estimadas, $tarea->tiempo_transcurrido, $tarea->puntaje);
			$dataRanking = array(
				'puntaje' 					=> $calRanking['puntajeObtenido'],
				'porcentaje_cumplimiento' 	=> $calRanking['procentajeCumplimiento'],
				'tarea_id' 					=> $tareaId,
				'usuario_id' 				=> $usuario
			);
			
			if($ranking = $this->tarea_model->getRanking($tareaId))
				$this->tarea_model->updateRanking($dataRanking, $ranking->id);
			else
				$this->tarea_model->saveRanking($dataRanking);
		}

		$data['success'] = $this->tarea_model->update_estado( $tareaId, $estado, $fecha );
		
		$historial = array(
			"historial" 	=> estadoByIdForHistorial( $estado ),
			"tarea_id" 		=> $tareaId,
			"estado_historico" => $estado,
			"fecha" 		=> date("Y-m-d H:i:s"),
			"tipo"			=> 1
		);
		$this->historial_model->save($historial);

		echo json_encode( $data );
	}
	
	public function getTablaTareas($user=false)
	{
		$start = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search')['value'];

		$tareas = false;
		if( !$user )
		{
			$dataWhere = array('tarea.estado_id !='=>8);
		}
		else
		{
			$dataWhere = array(
				'tarea.estado_id !=' => 8,
				'tarea.usuario_id' => $idUsuario
			);
		}
		
		$data = array();
		$result = $this->tarea_model->getAll($dataWhere, array('start'=>$start, 'length'=>$length, 'search'=>$search, 'limit'=>false));
		if($result['result']) 
		{
			foreach ( $result['result'] as $tarea ) 
			{
				$colorClass = "";
				$prioridad = "";
				switch ($tarea->prioridad_id) {
					case 1: $colorClassP = "badge-success"; $prioridad = "Baja"; break;
					case 2: $colorClassP = "badge-warning"; $prioridad = "Media"; break;
					case 3: $colorClassP = "badge-danger"; $prioridad = "Alta"; break;
				}
				$colorClassE = "";
				switch ($tarea->estado_id) 
				{
					case 1: $colorClassE = "bg-blue"; break;
					case 2: $colorClassE = "bg-orange"; break;
					case 3: $colorClassE = "bg-green"; break;
					case 4: $colorClassE = "bg-yellow"; break;
					case 5: $colorClassE = "bg-teal"; break;
					case 6: $colorClassE = "bg-purple"; break;
					case 7: $colorClassE = "bg-red"; break;
				}

				$data[] = array(
					'id' => $tarea->id,
					'informante' => "<div style='width:100%;'><span class='avatar' style='float:left; margin-right:10px; background-image: url(".base_url()."/assets/images/users/".$tarea->imagen.")'></span><span style='float:left;'>" . $tarea->nombre . "</span></div>",
					'codigo' => $tarea->codigo,
					'estado' => "<span class='badge ".$colorClassE."'>".$tarea->estado."</span>",
					'prioridad' => "<span class='badge ".$colorClassP."'>".$prioridad."</span>",
					'acciones' => "<button id-tarea='".$tarea->id."' title='Descripción de la tarea' class='btn btn-info detalle-tarea p-0'><i class='fe fe-list'></i></button>
									<button id-tarea='".$tarea->id."' title='Ver historia de la tarea' class='btn btn-info btn-historial p-0'><i class='fe fe-clock'></i></button>"
				);
			}
		}

		$data_json = array(
			'draw' => intval($this->input->post('draw')),
			'recordsTotal' => intval($result['rows']),
			'recordsFiltered' => intval($result['total_rows']),
			'data' => $data
		);
		echo json_encode($data_json);
	}

	public function getEstados()
	{
		$idTarea = $this->input->post('tarea');
		$prueba = $this->pruebaCruzada_model->getByTarea($idTarea);
		$tarea = $this->tarea_model->estaDetenida($idTarea);

		$response['estadoPrueba'] = ($prueba) ? $prueba->estado_id : false;
		$response['tareaDetenida'] = ($tarea) ? $tarea->detenida : false;

		echo json_encode($response);
	}

	public function getTareaMerged()
	{
		$start = $this->input->post('start');
        $length = $this->input->post('length');
		$search = $this->input->post('search')['value'];

		$data = array();
		$idUsuario = $this->session->userdata('id');
		$dataWhere = ($this->session->userdata('perfil_id') == 4 ) ?
			array('tarea.estado_id'=>6) :
			array('tarea.estado_id'=>6, 'tarea.usuario_id'=>$idUsuario);

		$tareas = $this->tarea_model->getAll($dataWhere, array('length'=>$length, 'start'=>$start, 'search'=>$search, 'limit'=>false));
		if ($tareas)
			foreach ($tareas['result'] as $tarea) 
			{
				$prioridad = "";
				$colorClassP = "";
				switch ($tarea->prioridad_id) {
					case 1: $colorClassP = "badge-success"; $prioridad = "Baja"; break;
					case 2: $colorClassP = "badge-warning"; $prioridad = "Media"; break;
					case 3: $colorClassP = "badge-danger"; $prioridad = "Alta"; break;
				}
				$data[] = array(
					'id' => $tarea->id,
					'informante' => "<div style='width:100%;'><span class='avatar' style='float:left; margin-right:10px; background-image: url(".base_url()."assets/images/users/".$tarea->imagen.")'></span><span style='float:left;'>".$tarea->nombre."</span></div>",
					'codigo' => $tarea->codigo,
					'prioridad' => "<span class='badge ".$colorClassP."'>".$prioridad."</span>",
					'acciones' => "<button id-tarea='".$tarea->id."' title='Descripción de la tarea' class='btn btn-info detalle-tarea p-0'><i class='fe fe-list'></i></button>"
				);
			}

		$json_data = array(
			'draw' => intval($this->input->post('draw')),
			'recordsTotal' => intval($tareas['rows']),
			'recordsFiltered' => intval($tareas['total_rows']),
			'data' => $data
		);

		echo json_encode($json_data);
	}

	public function correrPausar($flag=false)
	{
		$tarea_id = $this->input->post('tarea');
		$estado = $this->input->post('estado');
		$dataUpdate = array();
		$tarea = $this->tarea_model->getTarea($tarea_id);
		$historial = array();

		if ($estado == 'f')
		{
			if ($tarea)
			{
				$fechaInicio = $tarea->fecha_inicio;
				$fechaFin = $tarea->fecha_fin;

				$arrTime1 = array();
				$arrTime2 = array();
				$h = 0;
				$m = 0;
				$tiempoTrancurrido = $tarea->tiempo_transcurrido;

				if ($tiempoTrancurrido != "" || $tiempoTrancurrido != null)
				{
					$arrTime1 = explode(":", $tiempoTrancurrido);
					$h = (int)$arrTime1[0];
					$m = (int)$arrTime1[1];
				}

				$time = tiempoTranscurrido($fechaInicio, date("Y-m-d H:i:s"));
				$arrTime2 = explode(":", $time['time']);

				$h = $h + (int)$arrTime2[0];
				if ($m + (int)$arrTime2[1] > 59)
				{
					$h++;
					$m = ($m + (int)$arrTime2[1]) - 59;
				}
				else
				{
					$m = $m + (int)$arrTime2[1];
				}
				$horasT = $h < 10 ? "0".$h : $h;
				$minT = $m < 10 ? "0".$m : $m;

				$dataUpdate = array(
					'detenida' 				=> true, 
					'fecha_fin' 			=> date("Y-m-d H:i:s"),
					'tiempo_transcurrido' 	=> $horasT.":".$minT
				);

				$historial = array(
					"historial" 	=> "Tarea en detenida",
					"tarea_id" 		=> $tarea_id,
					"estado_historico" => $tarea->estado_id,
					"fecha" 		=> date("Y-m-d H:i:s"),
					"tipo"			=> 1
				);
			}
		}
		else
		{
			$dataUpdate = array(
				'detenida'=>false, 
				'fecha_inicio'=>date("Y-m-d H:i:s")
			);
			$historial = array(
				"historial" 	=> "Tarea en reanudada",
				"tarea_id" 		=> $tarea_id,
				"estado_historico" => $tarea->estado_id,
				"fecha" 		=> date("Y-m-d H:i:s"),
				"tipo"			=> 1
			);
		}

		if ($flag == false)
			$this->historial_model->save($historial);

		$data['resp'] = $this->tarea_model->correrPausar($tarea_id, $dataUpdate);
		echo json_encode( $data );
	}
}
