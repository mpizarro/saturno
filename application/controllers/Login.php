<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct ()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('login_model');
    }

    public function index ()
    {
        $session = $this->session->userdata("username");
        $perfil = $this->session->userdata("perfil_id");
        if (isset($session))
            if ( $perfil == 4 )
                redirect('/administrador/pizarra');
            else if ( $perfil == 1 )
                redirect('/developer/pizarra');
            else if ( $perfil == 2 )
                redirect('/departamento/tickets');
            else
                redirect('/login/view');
        else
            redirect('/login/view');
    }

    public function view()
    {
        $this->load->view("login");
    }

    public function check_login ()
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $is_logged = $this->login_model->check_login($username, $password);

        $data['is_logged'] = $is_logged;
        echo json_encode($data);
    }

    public function log_out ()
    {
        session_destroy();
        redirect('/login/view');
    }
}
