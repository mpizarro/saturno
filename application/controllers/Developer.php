<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends CI_Controller {

	private $img = null;

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
        $this->load->helper('utilidades');
        $this->load->library('session');
        $this->load->model('tarea_model');
        $this->load->model('usuario_model');
        $this->load->model('historial_model');

		if( $this->session->userdata("username") == null || $this->session->userdata("perfil_id") != 1 )
			redirect("login/log_out");

		$imagen = $this->usuario_model->getImg( $this->session->userdata("id") )->imagen;
		$this->img = ( $imagen != null ) ? $imagen : "pony.jpg";
    }

    public function pizarra()
	{
		$data['menu']	 	 = get_menu( "pizarra" );
		$data['id_usuario']  = $this->session->userdata('id');
		$data['name_user']	 = $this->session->userdata('name');
		$data['perfil_label']= $this->session->userdata('perfil');
		$data['img'] = $this->img;
		
		$this->load->view('header', $data);
		$this->load->view('developer/menu');
		$this->load->view('pizarra');
		$this->load->view('footer');
	}

	public function testing ()
	{
		$data['menu']	 	 = get_menu( "testing" );
		$data['id_usuario']  = $this->session->userdata('id');
		$data['name_user']	 = $this->session->userdata('name');
		$data['perfil_label']= $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view('header', $data);
		$this->load->view('developer/menu');
		$this->load->view('testing');
		$this->load->view('footer');
	}
}