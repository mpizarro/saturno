<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PruebaCruzada extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('utilidades');
		$this->load->library('session');
		$this->load->model('tarea_model');
		$this->load->model('pruebaCruzada_model');
		$this->load->model('usuario_model');
		$this->load->model('historial_model');

		if($this->session->userdata("username") == null)
			redirect("login/log_out");
	}

	public function asignarPruebaCruzada()
	{
		$solicitante = $this->session->userdata('id');
		$solicitado = $this->input->post('solicitado');
		$tarea = $this->input->post('tarea');

		$data_where = array( 
			'tarea_id'=>$tarea
		);
		$nPruebas = $this->pruebaCruzada_model->countPruebaCruzada($data_where);

		$dataInsert = array(
			'usuario_solicitante' => $solicitante,
			'usuario_solicitado' => $solicitado,
			'fecha' => date("Y-m-d H:i:s"),
			'tarea_id' => $tarea,
			'estado_id' => 10
		);

		if ($nPruebas == 0) {
			$data['resp'] = $this->pruebaCruzada_model->asignarPruebaCruzada($dataInsert);
		} else {
			$data['resp'] = $this->pruebaCruzada_model->updatePruebaCruzada($dataInsert, $data_where);
		}

		$usuarioSolicitante = $this->usuario_model->getUsuario($solicitante)->nombre;
		$usuarioSolicitado = $this->usuario_model->getUsuario($solicitado)->nombre;
		$historial = array(
			"historial" 	=> "<b>" . $usuarioSolicitante . "</b> solicitó prueba cruzada a <b>" . $usuarioSolicitado . "</b>",
			"tarea_id" 		=> $tarea,
			"estado_historico"	=> 4,
			"fecha" 		=> date("Y-m-d H:i:s"),
			"tipo"			=> 1
		);
		$this->historial_model->save($historial);

		echo json_encode($data);
	}

	public function getPruebaCruzada()
	{
		$estado = $this->input->post('estado');
		$idUser = $this->session->userdata('id');
		$perfilId = $this->session->userdata('perfil_id');

		$data['rows'] = "";

		$pruebasCruzadas = ($perfilId == 4) ?
			$this->pruebaCruzada_model->getAll() : 
			$this->pruebaCruzada_model->getByUser($idUser);

		if ($pruebasCruzadas) {
			foreach ($pruebasCruzadas as $pruebaC) {
				$usuarioSolicitado = $this->usuario_model->getUsuario($pruebaC->usuario_solicitado);
				$nombreUsuarioSolicitado = "";
				if($usuarioSolicitado) {
					$nombreUsuarioSolicitado = $usuarioSolicitado->nombre;
					$imagenSolicitado = $usuarioSolicitado->imagen;
				}
				$colorClass = "";
				$prioridad = "";
				$botonAccion = "";
				$estado = "";
				switch ($pruebaC->prioridad_id) {
					case 1: $colorClass = "badge-success"; $prioridad = "Baja"; break;
					case 2: $colorClass = "badge-warning"; $prioridad = "Media"; break;
					case 3: $colorClass = "badge-danger"; $prioridad = "Alta"; break;
				}
				 
				switch ($pruebaC->estado_id) {
					case 10:
						if ( $idUser == $pruebaC->usuario_solicitado )
							$botonAccion = "<button id='".$pruebaC->id."' estado='11' title='Aceptar prueba cruzada' class='btn btn-success btn-change-estado p-0'><i class='fe fe-git-branch'></i></button>";
						$estado = "<span class='badge bg-yellow'>PENDIENTE</span></td>";
						break;
					case 11:
						if ( $idUser == $pruebaC->usuario_solicitado )
							$botonAccion = "<button id='".$pruebaC->id."' title='finalizar prueba cruzada' class='btn btn-warning btn-modal-finalizar p-0'><i class='fe fe-git-merge'></i></button>";
						$estado = "<span class='badge bg-teal'>EN TESTING</span></td>";
						break;
				}
				
				$data['rows'] .= "<tr id='row-".$pruebaC->id."'>
					<td class='w-1'>
					<span class='avatar' style='background-image: url(".base_url()."assets/images/users/".$pruebaC->imagen.")'></span>
					</td>
					<td>".$pruebaC->solicitante."</td>
					<td class='w-1'>
					<span class='avatar' style='background-image: url(".base_url()."assets/images/users/".$imagenSolicitado.")'></span>
					</td>
					<td>".$nombreUsuarioSolicitado."</td>
					<td>".$pruebaC->codigo."</td>
					<td class='text-nowrap'>
						<span class='badge ".$colorClass."'>".$prioridad."</span></td>
					<td class='text-nowrap'>
						".$estado."
					<td>
						<button id='".$pruebaC->id_tarea."' title='Descripción de la tarea' class='btn btn-info btn-descripcion p-0'><i class='fe fe-list'></i></button>
						".$botonAccion."
					</td>
				</tr>";
			}
		}
		echo json_encode($data);
	}
	
	public function changeEstado()
	{
		$idPruebaCruzada = $this->input->post('idPruebaCruzada');
		$estado = $this->input->post('estado');
		$observacion = $this->input->post('observacion');

		$data['resp'] = $this->pruebaCruzada_model->changeEstado($idPruebaCruzada, $estado, $observacion);
		$tarea = $this->pruebaCruzada_model->getIdTareaByPruebaCruzada($idPruebaCruzada);

		$idTarea = ($tarea) ? $tarea->tarea_id : 0;
		$idUser = $this->session->userdata('id');

		$usuarioSolicitado = $this->usuario_model->getUsuario($idUser)->nombre;
		$historial = array(
			"historial" 	=> $usuarioSolicitado . " acepta prueba cruzada",
			"tarea_id" 		=> $idTarea,
			"estado_historico"	=> 4,
			"fecha" 		=> date("Y-m-d H:i:s"),
			"tipo"			=> 1	
		);
		$this->historial_model->save($historial);
		echo json_encode($data);
	}

	public function delete()
	{
		$tarea = $this->input->post('tarea');

		$dataWhere = array('tarea_id'=>$tarea);
		$response['response'] = $this->pruebaCruzada_model->delete($dataWhere);

		echo json_encode($response);
	}

	public function getObservacion()
	{
		$idTarea = $this->input->post('tarea');
		$response['observacion'] = 'Sin observaciones...';
		$prueba = $this->pruebaCruzada_model->getByTarea($idTarea);
		if ( $prueba )
			$response['observacion'] = $prueba->observacion !== "" ? $prueba->observacion : 'Sin observaciones...';

		echo json_encode($response);
	}
}
