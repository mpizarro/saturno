<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends CI_Controller {

	private $img = null;

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
        $this->load->helper('utilidades');
        $this->load->library('session');
        $this->load->model('usuario_model');

		if( $this->session->userdata("username") == null || $this->session->userdata("perfil_id") != 2 )
			redirect("login/log_out");

		$imagen = $this->usuario_model->getImg( $this->session->userdata("id") )->imagen;
		$this->img = ( $imagen != null ) ? $imagen : "pony.jpg";
    }

    public function tickets()
	{
		$data['menu'] = get_menu( "tickets" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'departamento/menu');
		$this->load->view( 'tickets');
		$this->load->view( 'footer');
	}

	public function historial()
	{
		$data['menu'] = get_menu( "historial_tickets" );
		$data['name_user'] = $this->session->userdata('name');
		$data['perfil_label'] = $this->session->userdata('perfil');
		$data['img'] = $this->img;

		$this->load->view( 'header', $data );
		$this->load->view( 'departamento/menu');
		$this->load->view( 'historial-tickets');
		$this->load->view( 'footer');
	}
}